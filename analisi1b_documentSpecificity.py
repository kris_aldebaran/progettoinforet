from pymongo import MongoClient as MC
from gensim import corpora
from util import Counter , Kappa
import re
vp = 0
fp = 0
vn = 0
fn = 0

if __name__ == "__main__":
    client = MC()
    db = client.rumors2


    c = Counter(tick = 50)
    f = open("output/specificitydoc.csv","w")
    f.write("URL,spec,class\n")
    
    regx = re.compile("/.*cicap.*/")
    
    for w in  db.wordsXdocs.find({"_id": {"$not": regx}}):
        c.update()
        tokens = w["words"]
        feat1 = db.feature1Specificity.find({"token":{"$in":tokens}})
        val = sum([ x["spec"] for x in feat1 ])
        if(val>0 and w["evaluation"]>0):
            vp += 1
        elif (val>0 and w["evaluation"]<=0):
            fp += 1
        elif (val<=0 and w["evaluation"]<=0):
            vn += 1
        elif (val<=0 and w["evaluation"]>0):
            fn += 1
        f.write('"{}",{},{}\n'.format(w["_id"],val,w["evaluation"]))
    f.close()
    
    print "veri positivi: ", vp
    print "falsi positivi: ", fp
    print "veri negativi: ", vn
    print "false negativi: ", fn
    print "errore: " , 100 - 100*(vp+vn+0.0)/(vp+vn+0.0+fn+fp) , " %"
    print "precision" , 100*(vp+0.0)/(vp+fp)
    print "recall" , 100*(vp+0.0)/(vp+fn)
    print "kappa" , Kappa(vp,fn,fp,vn)
    