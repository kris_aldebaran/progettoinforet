from time import time
class Counter:
    def __init__(self , tick= 500):
        self.i = 0
        self.tick = tick
        self.t = time()
        print "started"
        
    def update(self):
        self.i+=1
        if(self.i% self.tick == 0):
            print self.i , " ----> " , time() - self.t
            self.t = time()
            
def Kappa(vp,fn,fp,vn):
    pr_a = 0.0
    pr_e = 0.0
    tot = vp + vn + fp + fn
    pr_a = (vp + vn )/ float(tot)
    pr_e = (vp + fn)*(vp+fp) + (fp + vn)*(fn+vn)
    pr_e = (pr_e+0.0)/(tot ** 2.0)
    return (pr_a-pr_e)/(1.0 - pr_e)