﻿from bs4 import BeautifulSoup as BS
import time
import urllib2
from pymongo import MongoClient as MC
from pymongo.errors import DuplicateKeyError
client = MC()
db = client.rumors2

base = "http://www.ansa.it"
def readLink(url):
    
    response = urllib2.urlopen(url)
    return response.read()

def getData(hentry):
    getData.h = "data"
    li = hentry
    return str(li.ul.find_all("li")[1].next.split(" ")[0])
    
def getTitle(hentry):
    getTitle.h = "title"
    li = hentry
    return str(li.a.next)
    
def getLink(hentry):
    getLink.h = "_id"
    li = hentry
    return base+str(li.a["href"])
def getSubTitle(hentry):
    getSubTitle.h = "subTitle"
    li = hentry
    return li.p.next.next.decode(encoding='UTF-8')
    
def getEvaluation(hentry= None):
    getEvaluation.h = "evaluation"
    return 0
    
def getBody(hentry):
    getBody.h = "body"
    text = readLink(getLink(hentry))
    soup2 = BS(text)
    x = soup2.find("div" , {"id" : "content-corpo"})
    if(x!= None):
        return x.getText()
    else :
        return ""
example = '''    
<li class="hentry">
<ul class="hentry-meta">
<li class="pos">
<strong><abbr title="posizione nella lista risultati">44</abbr></strong>
</li>
<li>18-03-2016 10:02</li>
</ul>
<h4>
<a href="/scienza/notizie/rubriche/biotech/2016/03/18/nelle-popolazioni-del-pacifico-doppio-dna-degli-antenati-delluomo-_810ee990-3346-449f-96da-c476e6c79d8d.html">Nelle popolazioni del Pacifico 'doppio' Dna degli antenati dell’uomo </a>
</h4>
<p></p><p>Il Dna di antenati dell'uomo moderno estinti da tempo, come l'uomo di Denisova e quello di Neanderthal, sopravvive ancora in alcune popolazioni della Melanesia, nel Pacifico a Nord dell'Australia. I geni ereditati da questi uomini primitivi sono ancora attivi</p><p></p>
</li>
'''
f = open("Ansa_sicence.txt")
soup = BS(f.read()) 
f.close() 
hentries = []
for li in soup.find_all('li'):
    if(li.get('class') != None and 'hentry' in li.get('class') ):
        hentries.append(li)  
print len(hentries)       
functions = [getData,getTitle,getSubTitle,getEvaluation,getBody]    

count = 0
for li in hentries:
    try:
        data = {funct.h:funct(li) for funct in functions}
        data["_id"] = getLink(li)
    except UnicodeEncodeError:
        continue
    try:
        db.raw.insert_one(data)
        print "done"
    except DuplicateKeyError:
        print "finded duplicate"
    except Exception:
        data["link"] = data["_id"]
        data["_id"] = data["_id"][-50:]
        try:
            db.raw.insert_one(data)
            print "done changed id"
            
        except Exception:
            print  data["link"]
            