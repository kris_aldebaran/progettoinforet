﻿data = [{
    "_id" : "http://www.dionidream.com/bobina-bac/" ,
    "title" : "Bobina BAC – Ecco come costruirla per risparmiare su acqua, gas e carburante",
    "subTitle" : "Chi l’ha provata ne è entusiasta e l’ha installata sui tubi della macchina, dell’acqua e gas di casa sperimentando meno consumi e più efficienza. Ecco come funziona e come costruirla ed istallarla." , 
    "disclaimer":"Questo articolo non è destinato a fornire consigli medici, diagnosi o trattamento.",
    "data" : "22-01-2016",
    "evaluation": 4,
    "body"  : '''Si chiama Bobina  B.A.C ( Bobina Bifiliare Chiusa Autoinduttiva ) ed è basato su due brevetti, uno di Nikola Tesla e l’altro di un ingegnere indiano Velagapudi M. Rao. E’ un dispositivo passivo che sembrerebbe si autoalimenti dalle frequenze presenti nell’aria e dalle cariche elettriche presenti nei fluidi, migliorandone la qualità.
Questo dispositivo può essere fatto facilmente in casa ed installato da sé. Chi non fosse pratico può mostrare i video seguenti ad un addetto ai lavori che ve le monterà dove volete. Prima di spiegarti come fare, è importante che tu conosca tutti i benefici indotti da questa fantastica bobina.
EFFETTI BOBINA BAC SULL’AUTO
La bobina può essere installata su tutti i tipi di auto, benzina, diesel, gpl e metano. Va installata prima e/o dopo il filtro carburante.
Risparmio carburante del 10-30%
Più potenza del motore
Migliore combustione del carburante a tutti i regimi.
Progressiva Pulizia del motore e della marmitta da depositi di combustioni incomplete.
Minor usura della marmitta catalitica perché ottimizzando la combustione, non lascia residui, produce molto meno sostanze tossiche che dovrebbe assorbire il catalizzatore.
Il motore ritorna quasi come nuovo nell’arco di 2 settimane circa, cioè il
tempo necessario utilizzando l’auto tutti i giorni, perchè si verifica l’effetto di
pulizia totale.
EFFETTI BOBINA BAC SULLE TUBATURE DELL’ACQUA
Va installata all’ingresso del tubo dell’acqua che dal contatore va in tutta la casa. Può poi essere anche installata per un effetto maggiore sulle tubature dell’acqua dei singoli rubinetti, come quelli della cucina, lavandino, doccia, lavatrice, ecc.
EFFETTO BOBINA BAC SULLE TUBATURE DEL GAS
Risparmio sulla bolletta del gas
I fornelli della cucina sono più potenti
Migliora l’efficienza della caldaia
Si stanno studiando gli effetti di tale bobina installata sulle piante e sull’uomo.
ISTRUZIONI SU COME COSTRUIRE LA BOBINA DI BAC
Prendi un doppino stereo di 1 metro con sezione 1 mm².
Fissa un estremità sul tubo con del nastro isolante.
Avvolgi il filo sul tubo in modo sempre DESTROGIRO, IN SENSO ORARIO VERSO DESTRA , sempre nel verso che segue l’acqua. (Questo punto è cruciale, altrimenti non funziona: pollice destro direzione del flusso e dita chiuse della mano come verso di avvolgimento)
Fai 13 giri sul tubo
Arrivato alla fine cerca di comprimere bene i fili che devono essere il più compatti possibile
Fissala con del nastro isolante
Gira i fili verso il capo opposto fino a sovrapporli in lunghezza almeno di 5 cm.
Rimuovere con una forbice l’isolamento di plastica ai capi del filo per almeno 2 cm.
Unire assieme il rosso iniziale e il nero finale, e il nero iniziale con il rosso finale e nastrare il tutto.
Nei seguenti video potrai capire meglio quanto descritto.
VIDEO ESPLICATIVI SU COME FARE LA BOBINA DI BAC E COME INSTALLARLA
Alcuni video sono più vecchi e altri più recenti. E’ importante sapere che ad oggi è stato osservato che si hanno effetti maggiori con bobine a 13 giri con sezione di 1 mm².
Spiegazione introduttiva sulla Bobina di Bac, quali sono i materiali necessari e come costruirla
Su internet ci sono tantissime testimonianze di persone entusiaste di come “un mucchio di fili” possa fare tutto questo. Da quanto sappia non ci sono test scientifici (finanziati da chi?) che dimostrano tutto questo, ma è ispirato a questo brevetto consultabile Burner fuel line enhancement device. Conosco molti ingegneri che non ne avevano mai sentito parlare e incuriositi l’hanno installata constatando che effettivamente funziona.
Per chi volesse approfondire, leggere testimonianze e ricevere direttamente il kit per costruirla (che si trova altrimenti in ogni ferramenta) può visitare il sito Ecocreando dove c’è un gruppo di persone che stanno testando con diversi apparecchi l’efficacia di questo dispositivo realizzabile gratuitamente da tutti.
Voglio far notare che è assolutamente non invasiva dato che viene installata esternamente ai tubi, quindi se non funzionasse significa che rimane tutto uguale a prima. Inoltre poiché il costo è di un paio di euro bisogna provarla.
Ringrazio il mio amico Simone che me l’ha fatta conoscere e che è entusiasta per i benefici che ne sta avendo. Io l’ho appena installata e vi terrò aggiornati nei commenti sulle mie impressioni.
Fatemi sapere la vostra esperienza nei commenti.'''
},

{
    "_id" : "http://www.wired.it/scienza/ecologia/2016/03/15/pollosauro-dinosauro/",
    "title": "Gli scienziati che stanno ricreando un pollosauro",
    "subTitle" : "Nell’Università del Cile un team di ricercatori è riuscito a riprodurre le gambe posteriori di un dinosauro manipolando geneticamente un embrione di pollo" ,
    "data" : "15-03-2016",
    "evaluation": 3 ,
    "body" : '''Circa 65 milioni di anni fa, una serie di eventi catastrofici portarono all’estinzione oltre il 76% delle specie viventi che abitavano la Terra, ponendo fine al dominio dei dinosauri. Non tutti gli antichi rettili sparirono però in quell’occasione: oggi sappiamo infatti che i dinosauri aviari sopravvissero alla catastrofe, colonizzando nuovi ecosistemi e nicchie biologiche, evolvendosi e trasformandosi pian piano in quelli che oggi chiamiamo uccelli.
Per questo, diversi team di ricerca in tutto il mondo stanno cercando di riportare in vita gli antichi dinosauri utilizzando gli embrioni dei loro discendenti moderni, per comprendere meglio come avvenne questa transizione. Un passo in avanti importante in questa direzione arriva oggi dall’Università del Cile, dove un team di ricercatori è riuscito a riprodurre le gambe posteriori di un dinosauro, manipolando geneticamente la sviluppo di un embrione di pollo.
Il loro studio in effetti è solo l’ultima di una lunga serie di ricerche che puntano a creare il cosiddetto pollosauro (o Chickenosaurus), ossia qualcosa di simile a un dinosauro ottenuto manipolando geneticamente un pollo per fargli recuperare i tratti atavici dei suoi lontani antenati, ancora presenti nello stadio embrionale degli uccelli.
Un campo di ricerca che vanta un pioniere illustre: il paleontologo Jack Horner, uno dei padri del moderno studio dei dinosauri, nonché consulente scientifico di tutti i film della serie di Jurassic Park.
Nel nuovo studio, pubblicato sulla rivista Evolution, il team di ricercatori cileni si è concentrato sul perone, osso della gamba che presenta importanti differenze tra uccelli e dinosauri: negli antichi rettili volanti, come l’Archaeopteryx, raggiungeva infatti la caviglia (come nella nostra specie), mentre negli uccelli ha una forma più affusolata e termina prima della caviglia.
Negli embrioni dei moderni volatili però, tibia e perone presentano ancora esattamente la stessa forma di quelli degli antichi dinosauri, ed è solo durante lo sviluppo che il perone modifica le sue fattezze. Studiando i meccanismi molecolari attivi nella parte terminale dell’osso durante questo sviluppo, i ricercatori cileni hanno notato l’attività precoce di un gene, chiamato Indian Hedgehog, responsabile della maturazione cellulare e coinvolto nei meccanismi che bloccano la replicazione delle cellule.
I ricercatori hanno quindi bloccato sperimentalmente l’attività del gene Indian Hedgehog in diversi embrioni di pollo, dimostrando che in questo modo il perone assume una forma allungata, più simile a quella dei dinosauri, e raggiunge la caviglia. La tibia inoltre risulta più corta del normale, un particolare che ha permesso ai ricercatori di individuare l’antenato degli uccelli che potrebbe aver sviluppato per primo il nuovo tipo di perone: un gruppo di uccelli con becchi dotati di pseudo-denti, vissuti nel cretaceo superiore al fianco dei dinosauri.''' 
},

{
    "_id" : "http://www.repubblica.it/salute/medicina/2016/01/11/news/misteriosa_omeopatia_utile_e_non_scientifica-131018197/",
    "title" :"Misteriosa omeopatia: utile e non scientifica",
    "subTitle" : "Medicina dolce. C'è troppa discrezionalità da parte del medico. Che invece deve essere coscienzioso e riconoscere i limiti della terapia",
    "data" : "11-01-2016",
    "evaluation": 4 ,
    "body" : '''Quando un antibiotico non ha gli effetti voluti, si cambia. Ma quando non funziona un farmaco omeopatico si dice che non funziona l'omeopatia tout court. La riflessione di Valérie Poinsot, direttore generale dei laboratori Boiron, azienda leader con 620 milioni di euro di fatturato annuo, apre il congresso di Praga, organizzato dal Cedh, la più importante scuola di omeopatia a livello mondiale, dedicato all'uso di questa medicina in pediatria. Oltre quattrocento medici arrivati da sedici paesi per discutere di diagnosi e scelta della terapia nei bambini.
Le patologie. Le principali indicazioni dell'omeopatia in pediatria sono - secondo Antoine Demonceaux, direttore Cedh, "le patologie otorinolaringoiatriche e delle vie respiratorie, come otiti, bronchiti e bronchioliti, i disturbi del comportamento e l'oncologia, con trattamenti che leniscono gli effetti collaterali della chemio".
Ma l'omeopatia ha dato anche risultati a volte insperati, come quelli ottenuti da Robert Dumont (della Northwestern University) nel suo centro che ha in cura oltre 3000 bambini autistici. Dumont ha osserveto miglioramenti per sintomi come aggressività, collera, dondolamenti, difficoltà di concentrazione. Com'è potuto accadere? Non si sa. Perché l'omeopatia funzioni e con quale meccanismo - hanno sostenuto molti medici - non è chiaro, ma l'importante è avere risultati.
Dimostrare l'efficacia. Il problema della dimostrazione di efficacia - fondamentale - è quello che ha spinto il servizio sanitario britannico a ripensare al rimborso delle cure a spese pubbliche. "Una decisione che mi stupisce ma che capisco - premette Francesco Macrì, professore di pediatria alla Sapienza di Roma e vicepresidente Siomi - in tempi di crisi non si investe in settori che non riescono a dare prove di efficacia. Ma è un cane che si morde la coda, perché gli studi clinici dovrebbero essere svolti in strutture pubbliche, dove ci sono grandi difficoltà, a partire dai comitati etici che difficilmente danno il nullaosta. Se si dovesse poi superare questa difficoltà c'è il problema della pubblicazione, poiché le grandi riviste tendono a rifiutare lavori scientifici sull'omeopatia".
Mancanza di linee guida. Un altro aspetto rilevante è quello dell'assenza di linee guida per diagnosi e prescrizioni. "Con una forte discrezionalità del medico - continua Macrì - per cui, essendoci a disposizione per lo stesso sintomo diversi rimedi la scelta efficace è garantita dalla buona preparazione e dall'adeguata esperienza di chi prescrive, in modo da evitare l'errore di una prescrizione omeopatica errata oppure in caso di malattie gravi che riconoscono solo l'uso della terapia convenzionale, come l'insulina in caso di coma diabetico per esempio. Un medico coscienzioso deve essere in grado di riconoscere i limiti dell'omeopatia e individuare le situazini in cui ha buone possibilità di successo, quelle in cui può affiancare la medicina convenzionale, come nella terapia di supporto dell'asma al di fuori degli episodi acuti in cui i broncodilatatori sono insostituibili, e quelli in cui i risultati sono scarsi".
La Società italiana di pediatria ha da tempo un gruppo di studio sulle medicine non convenzionali, visto che, secondo una indagine della società, un terzo dei pediatri italiani le pratica più o meno spesso, in esclusiva o in affiancamento, e due terzi le conosce.
'''
},



{
    "_id" : "http://www.repubblica.it/tecnologia/mobile/2016/02/15/news/una_rete_sismica_mondiale_realizzata_con_gli_smartphone-133467932/" ,
    "title": "Una rete sismica mondiale. Realizzata con gli smartphone" , 
    "subTitle" : "L’Università di Berkeley in California ha realizzato una app, MyShake, in grado di rilevare i movimenti sismici attraverso il telefono cellulare. Un modo per raccogliere dati in tutto il mondo e, in futuro, riuscire ad avvisare le popolazioni di un evento",
    "data" : "16-02-2016",
    "evaluation": 2  ,
    "body" :'''SI CHIAMA MyShake ed è una app che sfrutta la possibilità degli smartphone di registrare le vibrazioni sismiche e condividerle in una cloud mondiale. Potrebbe presto allertare i possessori della app di un terremoto che si è sviluppato a chilometri di distanza e che è in arrivo. Pochi secondi, però attimi preziosi per ripararsi. Lo strumento, per ora un prototipo, è disponibile su Google Play Store.
    Gli sviluppatori spiegano che questa forma di rete non sostiuirà le reti sismiche nazionali, decisamente più precise e affidabili ma “potrebbe essere l’unica opzione in molti Paesi in via di sviluppo, suscettibili a terremoti ma con reti sismiche ancora limitate, se non del tutto assenti, e senza sistemi efficienti per allertare la popolazione”. In queste aree, come il Nepal o il Peru, mancheranno le reti sismiche, ma non certo i telefoni cellulari.
    Oggi gli smartphone sono sufficientemente sensibili per riconoscere il sussulto di terremoti con magnitudo superiore a 5, ovvero quelli più dannosi e pericolosi.ome funziona dunque MyShake? Quasi tutti gli smartphone possiedono un accelerometro. Il suo scopo principale è quello di riconoscere l’orientazione del cellulare per riposizionare correttamente il visore, oppure per giochi come il simulatore di volo.
Secondo i ricercatori che hanno sviluppato il sistema, se è vero che la sensibilità non è paragonabile a quella degli strumenti geofisici, è però vero che il numero di smartphone che ci sono in giro è enorme. E, una volta “insegnato” agli smartphone a distinguere le vostre attività consuete da un terremoto, grazie all’algoritmo di MyShake, gli smartphone si trasformano in un rudimentale sismometro.
Pur funzionando in background i suoi consumi sono bassissimi, assicurano gli sviluppatori. In questo modo gli smartphone possono misurare tremori giorno e notte senza svuotare la batteria in pochi minuti. Per il momento la app raccoglie le vibrazioni del telefono, mentre un algoritmo distingue una sorta di “firma” caratteristica del terremoto (diversa dalla vibrazione della camminata, del trattore, da una caduta accidentale, o altre ancora). Se lo strumento sospetta che sia avvenuto un terremoto registra 
le coordinate gps dello strumento e invia i dati a un server centrale per le analisi via wifi o rete cellulare. Se un numero sufficiente di telefoni riconosce un sisma, un allarme potrebbe essere lanciato pochi secondi prima che l’onda sismica raggiunga centri urbani a chilometri di distanza. Il sistema è ancora in fase di test, ma i ricercatori confidano nella diffusione della app per poterlo raffinare e rendere infine un utile strumento di allerta.
In futuro gli scienziati dell’Università di Berkeley in California contano di poter avvertire in pochi secondi gli utenti di un terremoto che si è verificato a chilometri di distanza. “In California, una delle regioni con la miglior rete sismografica del mondo, abbiamo 400 stazioni sismiche”, spiega Allen. “Basterebbe che anche una piccola parte dei 16 milioni di telefoni cellulari partecipasse al nostro programma per poter avere a disposizione un’immensa quantità di dati sismici”.
a app per ora è disponibile solo per Android, ma ci sarà presto anche una versione per iPhone.
L’algoritmo di MyShake è stato presentato sulla rivista Science Advances da Richard Allen, Louis Schrier, e Young-Woo Kwon, della Università della California di Berkeley e del Centro per l’Innovazione della Deutsche Telekom, presso la Silicon Valley.'''
    
   
},
{
    "_id" : "http://www.butac.it/piccole-perle-di-facebook-le-penne-milionarie-della-nasa/",
    "evaluation": 4  ,
    "body":'''Si racconta che la NASA, alle prese col problema di scrivere nello spazio in assenza di peso, abbia speso milioni di dollari per realizzare una biro col serbatoio d'inchiostro pressurizzato. Senza la pressurizzazione e senza la gravità a farlo scendere, infatti, l'inchiostro non scorreva verso la punta e quindi la biro non scriveva.
Gli ingegneri sovietici, dovendo risolvere lo stesso problema, usarono la loro proverbiale semplicità.
Diedero ai cosmonauti una matita.'''
},

{
    "_id" : "http://www.ilgiomale.it/wordpress/secondo-un-evangelista-un-asteroide-distruggera-la-terra-a-settembre",
    "title": "Secondo un Evangelista un asteroide distruggerà la terra a Settembre." , 
    "subTitle" : "Un grosso asteroide colpira la terra a Settembre prossimo, precisamente tra il 22 e il 28, questa è la notizia, un pò promessa e un pò minaccia che sta facendo il giro del web, una notizia degna di Armageddon.",
    "data" : "10-07-2015",
    "evaluation": 4  ,
    "body" :'''Ma da dove arriva questo inquietante comunicato?
Non da un astronomo, nè da uno scienziato, ma da un Evangelista Cristiano Pentecostale, il Reverendo Rodriguez che ha scritto alla NASA anni fa, avvertendo l’agenzia spaziale americana del pericolo. Il Reverendo Rodriguez dice di aver appreso del disastro da fonti divine, che gli avrebbero anche rivelato il luogo dell’impatto, secondo lui infatti l’asteroide dovrebbe cadere nell’oceano vicino Porto Rico, provocando così disastri, terremoti e maremoti che devasteranno al costa orientale degli USA, Messico, America Centrale e anche quella del Sud.
A voi le conclusioni, ricordatevi che l’atmosfera terrestre ci protegge continuamente da questo tipo di minacce, esistono si delle eccezioni cone nel 1908 e nel 2013, ma dopo al profezia Maya, crederete che il 28 settembre possa succedere qualcosa del genere?
Il 28 settembre rimane comunque una importante data astronomica, ci sarà infatti la quarta eclissi di luna.
Su youtube viene spiegata la vicenda, e come e nata la notizia catastrofica, in questo video: '''
   
},

{
    "_id" : "http://www.libreidee.org/2015/09/paura-il-sole-va-in-letargo-sta-arrivando-unera-glaciale/" , 
    "title" : "Paura: il sole va in letargo, sta arrivando un’era glaciale", 
    "data" : "11-09-2015" ,
    "evaluation":  4 ,
    "body" : '''Caro presidente Obama, si copra bene: farà freddo, freddissimo, per almeno trent’anni. Lo sostiene John Casey, già climatologo della Nasa, ora direttore di un centro studi di Orlando, la “Space and Science Research Corporation”. L’allarme: l’attività del sole sta rapidamente cambiando, la comparsa di nuove macchie solari lascia presagire un nuovo “grande inverno” per il pianeta, a partire dal 2015-2016, con conseguenze devastanti, fenomeni di assideramento di massa e addirittura il crollo del 50% della produzione alimentare a causa del collasso dell’agricoltura terrestre. E’ il drammatico contenuto della lettera che lo scienziato ha rivolto al capo della Casa Bianca già lo scorso aprile. Oggetto: “Richiesta di preparare gli Stati Uniti per un pericoloso clima freddo”. Casey chiede di prendere «provvedimenti immediati per garantire che gli Stati Uniti d’ America siano pronti per lo storico e potenzialmente pericoloso clima freddo, che sta per arrivare». Ma il problema non era il global warming, il surriscaldamento? Sì, fino a ieri. Da oggi, l’allarme è di segno opposto: neve, gelo, temperature sotto zero. A partire dai prossimi mesi.
Casey cita le ricerche condotte negli ultimi decenni sulle cause del cambiamento climatico. «Il periodo passato del riscaldamento globale è un fenomeno naturale, prodotto principalmente dal Sole, ed è finito», scrive il climatologo nella sua Iceberglettera a Obama, ripresa dal blog “Daltonsminima”. «Sono oltre diciassette anni che non viene registrata alcuna effettiva crescita delle temperature atmosferiche globali in troposfera. Ironia della sorte, questo significa che, mentre per la maggior parte del tempo la comunità internazionale ha avuto a che fare con il riscaldamento globale, quest’ultimo, non c’era! E’ quindi importante accettare che il riscaldamento globale è finito». Non c’è più alcun riscaldamento globale, sostiene Casey. «La terra – scrive – è in fase di raffreddamento da diversi anni, come gli oceani negli ultimi undici anni e l’atmosfera per la maggior parte del tempo». Dei 24 parametri climatici monitorati dalla Ssrc, la “Space and Science Research Corporation”, registrati in report trimestrali, ben 18 di essi «mostrano un raffreddamento globale come tendenza dominante». Quanto ai restanti 6, «si stanno convertendo verso lo stato di raffreddamento, entro i prossimi cinque anni».
Lo scienziato dichiara che il livello del mare ha già iniziato a calare, dove alcune aree oceaniche stanno diventando più fredde. Il centro ricerche coordinato da Casey prevede una una riduzione globale del livello del mare della durata di 30 anni, «che inizierà in qualsiasi momento tra quest’anno e il 2020». Se queste tendenze cambiassero, il centro studi sarebbe il primo a segnalarlo, dice Casey. «Tuttavia, sulla base delle temperature globali effettive e i modelli climatici più affidabili, c’è una sola conclusione da effettuare sullo stato attuale clima della Terra: un nuovo clima freddo è arrivato». Se questa “nuova era glaciale” procedesse come negli episodi passati (circa 200 e 400 anni fa), secondo Casey «dovremmo aspettarci di vedere notevoli danni alle colture a livello mondiale, sconvolgimenti sociali e politici e la perdita della vita». Secondo gli studiosi, questi effetti New York sotto la nevecatastrofici «potrebbero iniziare presto e durare almeno tre decenni». Casey aggiunge che abbiamo poco tempo per prepararci: «Gli scienziati russi si sono spinti fino a parlare di una nuova “Piccola era glaciale” che inizierà quest’anno», il che significa che «il clima freddo, che avanza, è una grave minaccia per la nostra gente».
Secondo gli studiosi americani, il nuovo clima freddo verrebbe imposto alla Terra «da un ripetuto ciclo, di 206 anni, del sole». Casey l’aveva già annunciato nel 2007. «Anche se molti altri ricercatori hanno scoperto questo ciclo o previsto un clima freddo in arrivo, sono stati ignorati», scrive lo scienziato, nella sua lettera a Obama. «La fase di freddo, di questo lungo ciclo di due secoli, è prodotto dalla riduzione drammatica dell’energia con la quale il sole scalda la terra». Lo chiamano “letargo solare”. Ed è stato confermato dalla Nasa, dall’aviazione Usa e dal “National Solar Observatory”: parlano di “declino in corso dell’attività solare”. «La ricerca relativa su questi letarghi John Caseysolari – aggiunge Casey – mostra anche che si verificano in concomitanza con i terremoti e le eruzioni vulcaniche più distruttive, l’ultima delle quali può portare drammaticamente ad un clima già freddo».
Potenzialmente devastanti le conseguenze dello choc climatico sulla popolazione, a cominciare dalle categorie più esposte: «Credo che gli afro-americani, altre minoranze e i poveri soffriranno di più, per la nuova era fredda e le vostre politiche climatiche», scrive Casey al presidente Usa. «Questa affermazione è supportata dal fatto che una grande percentuale di questi cittadini sono in gran parte dipendenti dal governo degli Stati Uniti, per il cibo, che inizierà a diminuire, come il freddo comicerà a danneggiare le colture». Senza adeguate contromisure governative, avverte lo scienziato, «saremo impreparati e incapaci di procurarci il cibo di routine, durante gli anni peggiori del clima freddo in arrivo». Inutile aggiungere che ci sarà un’impennata dei costi dell’energia, micidiale soprattutto per i più poveri. Casey rimprovera a Obama di aver “creduto alla teoria del surriscaldamento globale”. E gli rinfaccia la responsabilità per l’incolumità di 317 milioni di cittadini americani di fronte al “grande freddo” che, giura, sta davvero per arrivare. '''
},
{
    "_id" : "http://www.dionidream.com/queste-lampadine-causano-mal-di-testa-ansia-e-anche-il-cancro-ecco-cosa-fare/", 
    "title" : "Queste lampadine causano mal di testa, ansia e anche il cancro. Ecco cosa fare", 
    "subTitle" : "Ci hanno detto che erano più ecologiche e che ci avrebbero fatto risparmiare sulla bolletta, ma hanno danneggiato la nostra salute. Le nuove lampadine a risparmio energetico possono essere davvero pericolose. Diversi studi hanno messo in guardia sul loro uso quotidiano e il pericolo maggiore è se si rompono tanto che la stessa Environmental Protection Agency ha creato un protocollo di emergenza da seguire in caso di rottura della lampadina proprio a causa del gas velenoso rilasciato. Vediamo cosa fare e perché sono così dannose.",
    "data" : "21-12-2015",
    "evaluation": 4  ,
    "body" : '''Le informazioni riportate riguardano le lampadine fluorescenti compatte (CFL) e non le lampadine a LED.
E’ stata riscontrata una correlazione tra le lampadine a risparmio energetico e i seguenti disturbi:
Vertigini
Cefalea a grappolo
Emicrania
Crisi epilettiche
Affaticamento
Difficoltà nella concentrazione
Ansia
Dermatite
Eczema
Autismo
Epilessia
Cancro
Quindi queste lampadine in casa fanno male, ma ancora di più se si rompono! Secondo uno studio, condotto dai ricercatori del Fraunhofer Wilhelm Klauditz Institute per l’Autorità Federale Ambientale in Germania: se rotte, queste lampadine rilasciano 20 volte la concentrazione massima accettabile di mercurio nell’aria.
Il mercurio, come ho trattato in molti miei articoli, è il metallo pesante più pericoloso per l’uomo e tossico a qualunque concentrazione. Diversi studi scientifici mostrano come esso danneggia irrimediabilmente il cervello e il sistema nervoso causando una miriadi di malattie gravi, ed inoltre depositandosi negli organi e ghiandole danneggia tutto il sistema ormonale e linfatico.
Perché le lampadine a risparmio energetico sono pericolose per la nostra salute?
Le lampadine a risparmio energetico contengono da 3 a 5 mg di mercurio.  Il mercurio è una potente neurotossina particolarmente pericolosa per i bambini e le donne in gravidanza. Questa sostanza è particolarmente tossica per il cervello, il sistema nervoso, il fegato e i reni. Può anche danneggiare il sistema cardiovascolare, immunitario e riproduttivo. Intossicazioni di mercurio possono causare perdita di memoria, cancro e Alzheimer.
Le lampadine a risparmio energetico possono causare il cancro. Un nuovo studio effettuato da Peter Braun presso il Germany’s Alab Laboratory ha evidenziato che questo tipo di lampadine contiene degli agenti cancerogeno-tossici in grado di causare il cancro:
Naftalene, un composto cristallino bianco volatile, prodotto dalla distillazione di catrame di carbone, utilizzato in naftalina e come materia prima per la produzione chimica.
Stirene, un idrocarburo insaturo liquido, ottenuto come sottoprodotto del petrolio.
Fenolo, un leggermente acido cristallino bianco tossico solido, ottenuto da catrame di carbone e utilizzato nella produzione chimica.
Le lampadine a risparmio energetico emettono raggi UV superiori alla norma. La Health Protection Agency (HPA) ha condotto uno studio e osservato che aumentano il rischio di cancro alla pelle soprattutto per chi lavora ore e ore vicino alle fonti di luce. È ufficialmente riconosciuta la pericolosità dei raggi UV per la nostra pelle e per gli occhi. Le radiazioni di queste attaccano direttamente il nostro sistema immunitario e impedisce la formazione adeguata di vitamina D.
Le lampadine a risparmio energetico generano potenti campi elettromagnetici a poca distanza dalla sorgente, fino ad un metro di distanza. Il centro indipendente di ricerche francese (CRIIREM) sconsiglia pertanto di utilizzare lampadine a basso consumo energetico a brevi distanze, come ad esempio per illuminare i comodini delle camere da letto o le scrivanie.
Il campo elettromagnetico generato da queste lampadine va in risonanza nei cavi elettrici generando “elettricità sporca” in tutta l’abitazione. Uno studio pubblicato nel giugno del 2008 dall’American Journal of Industrial Medicine segnalava che questa elettricità sporca aumenta di 5 volte il rischio di contrarre il cancro. Rimuovi l’elettricità sporca con il Filtro Vivar Gs.
Danneggiano la ghiandola pineale. Lo studio pubblicato su Chronobiology International, a cura del professor Abraham Haim, afferma che lo spettro luminoso di queste lampadine, essendo simile alla luce del giorno, interrompe la produzione di melatonina da parte dell’organismo. Cosa che invece non facevano le vecchie lampade a incandescenza. Gli effetti sono enormi dall’insonnia all’invecchiamento precoce, dalla depressione ad un aumento esponenziale del rischio di cancro, essendo la melatonina un potente antiossidante anticancro.
COSA FARE
Per prima cosa evita di avere queste lampadine in casa cercando le vecchie lampadine ad incandescenza oppure quelle nuove a LED (che però alla lunga stancano gli occhi e possono danneggiare la retina).  Sebbene siano state messe fuori produzione si possono ancora acquistare le vecchie lampadine online o nei negozi che hanno delle rimanenze di magazzino.
Se avete a casa le lampadine a risparmio energetico e si rompono devi stare molto attento nella pulizia e seguire questa procedura messa a punto dall’Environmental Protection Agency.
PULIZIA DI UNA LAMPADINA ROTTA – PROTOCOLLO EPA
Far evacuare la stanza se ci sono persone e animali domestici.
Arieggiare la stanza per 5-10 minuti aprendo la finestra o la porta a contatto con l’ambiente all’aperto.
Spegnere l’impianto di riscaldamento o di condizionamento dell’aria.
Non utilizzare l’aspirapolvere. L’aspirazione non è raccomandata perché potrebbe diffondere le particelle di mercurio presenti nella polvere.
Indossa i guanti, una mascherina e degli occhiali protettivi.
Raccogli i pezzi più grandi con le mani e i frammenti più piccoli con l’aiuto del nastro adesivo.
Riponi i frammenti della lampadina in contenitori ermetici, come vasi di vetro o sacchetti di plastica sigillabili.
Pulisci le superfici con un panno umido. Poi getta tutto ciò che avete utilizzato per la pulizia, inclusi il panno e i guanti.
Se la rottura avviene su un tappetino, eliminalo e rimuovi almeno la parte contaminata.
Chiamate il centro locale per la raccolta differenziata se hai dei dubbi sul da farsi. Porta i rifiuti presso la Piattaforma Ecologica del tuo Comune, in modo che siano smaltiti in modo corretto.
Come misura preventiva, sarebbe bene non utilizzare le lampadine al mercurio in aree a rischio di rottura e incidenti.
Lavati subito le mani quando hai terminato.
Una vecchia lampada ad incandescenza ci da sicuramente una luce più calda e gradevole delle nuove fredde luci a risparmio energetico. Potete acquistare su internet o in alcuni negozi che hanno rimanenze, ancora le lampadine ad incandescenza. Ecco un link dove potete comprarle online.

 '''
},
{
    "_id" : "http://www.evoluzionecollettiva.com/ricetta-olio-cannabis-rick-simpson-cura-cancro-e-altre-malattie/" , 
    "title" : "Rick Simpson: la mia ricetta per preparare l’Olio di Cannabis come cura contro il Cancro e altre malattie.", 
    "subTitle" :'''Una volta che le persone vengono a conoscenza del fatto che con la canapa è possibile curare e controllare praticamente qualsiasi patologia medica, chi può desistere dal provarla?
    Rick Simpson ha dedicato la sua vita ad aiutare le persone malate e sofferenti (di qualsiasi tipo di malattia e disabilità), con l’uso dell’olio di canapa naturale.
    L’olio di canapa di Rick Simpson si rivela efficace nella cura del cancro.''' ,
    "data" : "19-08-2015" ,
    "evaluation":  4 ,
    "body" : ''' Rick Simpson è un attivista della marijuana medica e da quasi un decennio fornisce alle persone informazioni dettagliate sulle proprietà curative dei medicinali prodotti con l’olio di canapa. Rick ha guarito se stesso da un melanoma metastatico nel 2003 e da allora ha dedicato la sua vita a diffondere la verità sull’olio di canapa. Lungo il suo percorso ha incontrato una quantità assurda di persone in opposizione e la totale mancanza di sostegno da parte delle autorità canadesi, come pure le aziende farmaceutiche, agenzie governative per la salute e uffici delle Nazioni Unite. Nonostante tutto, Rick Simpson ha trattato con successo oltre 5.000 pazienti (gratuitamente) e crede che tutte le forme di malattia e le condizioni fisiche siano curabili con l’olio di canapa. Egli afferma che è possibile curare tutti i tipi di cancro e di malattie mediante una terapia con olio di canapa di alta qualità.
Rick ha trattato pazienti di qualsiasi condizione medica, tra cui: cancro, AIDS, artrite, sclerosi multipla, diabete, leucemia, morbo di Crohn, depressione, osteoporosi, psoriasi, insonnia, glaucoma, asma, ustioni, emicranie, regolamento del peso corporeo, dolore cronico e mutazione cellulare (polipi, verruche e tumori).
“Run from the cure” il video del documentario ufficiale
Questo documentario “Run from the cure” è stato realizzato da Christian Laurette nel 2008 e racconta la storia di Rick Simpson. Il film contiene interviste con persone che sono state curate dall’olio di Rick, ma si sono rifiutate di testimoniare a favore di Rick nella Corte Suprema del Canada durante il suo processo del 2007. In questo documentario vengono spiegati in modo meticoloso tutti i benefici dell’olio di cannabis.
“Voglio che le persone sappiano come guarire se stesse.”
Rick Simpson ritiene che ingerire oralmente olio di canapa distrugga le cellule tumorali nel corpo. Quando viene utilizzato come topico, l’olio di canapa può controllare o addirittura curare varie malattie della pelle, quali i melanomi.
Tuttavia, come con qualsiasi “droga”, troppo olio di canapa può avere alcune controindicazioni e causare effetti collaterali; i tre più importanti sono: desiderio di cibo, euforia e sonnolenta. Si tratta di un farmaco estremamente sicuro rispetto ai centinaia di farmaci in commercio che sono stati approvati con poco o nessuno studio ed hanno recato ai pazienti orribili effetti collaterali, compresa la morte. Infatti, nessuno è mai morto dalla cannabis, somministrata in qualsiasi forma.
La ricetta dell’olio di cannabis di Rick Simpson
La prima volta che fai l’olio di cannabis inizia con un’oncia (circa 28,35 grammi) di canapa secca. Un’oncia produrrà circa 3-4 grammi di olio, anche se la quantità di olio prodotto per oncia varierà da ceppo a ceppo. Un chilo di materiale essiccato produrrà circa due once di olio di alta qualità.
IMPORTANTE: Queste istruzioni sono prese direttamente dal sito internet ufficiale di Rick Simpson. Fai molta attenzione durante l’ebollizione del solvente [opzioni del solvente], le fiamme sono estremamente infiammabili! EVITA il fumo, le scintille, i fornelli e le resistenze elettriche. Posiziona un ventilatore per soffiare via i fumi dalla pentola e allestisci un’area ventilata per tutto il processo.
Ecco il procedimento per produrre l’olio di cannabis:
1. Posiziona la canapa completamente asciutta in un contenitore di plastica.
2. Inumidisci l’erba con il solvente che hai deciso di utilizzare. Possono essere utilizzati molti solventi (opzione solventi). È possibile utilizzare nafta pura, etere, butano, 99% alcool isopropilico o anche acqua. Due litri di solvente sono necessari per estrarre il THC da un chilo, per un’oncia sono sufficienti 500 ml.
3. Schiaccia il materiale vegetale con un bastoncino di legno pulito non trattato o con qualsiasi altro utensile simile. Anche se il materiale sarà umido, riuscirai ancora facilmente a schiacciarlo perché ancora abbastanza asciutto.
4. Continua a schiacciare il materiale con il bastoncino e nel frattempo aggiungi il solvente, fino a quando il materiale vegetale sarà completamente coperto e bagnato. Continua a mescolare il composto per circa tre minuti. Mentre esegui questa operazione, il THC (uno dei maggiori e più noti principi attivi della cannabis) viene rilasciato e dissolto nel solvente.'''
},
{
    "_id" : "http://www.huffingtonpost.it/2015/12/16/nasa-asteroide-natale_n_8817214.html", 
    "title" : "L'annuncio della Nasa: La notte di Natale un asteroide sfiorerà la Terra", 
    "data" : "17-12-2015",
    "evaluation": 3  ,
    "body" : '''Il cielo natalizio avrà per protagonista una cometa, ma non la classica stellina codata, bensì un vero asteroide che punterà la Terra. Lo annuncia la Nasa che, grazie al programma NHATS (Near-Earth Object Human Space Flight Accessible Targets Study) ha rilevato 2003 SD220, il nome dell'asteroide.
L'agenzia spaziale americana ha dichiarato che il corpo passerà vicino al nostro pianeta - a circa 150 milioni di km -ma senza alcun pericolo per noi. Il passaggio è previsto nella notte del 24 dicembre a una distanza di 0,073 UA - una Unità Astronomica corrisponde a - pari a circa 27-28 volte la distanza della Terra dalla Luna. La Nasa ha promesso di fornire più dettagli nei prossimi giorni. Nel frattempo ha avvertito che nello stesso periodo un altro asteroide, molto più piccolo, potrebbe transitare anche più vicino alla Terra.
Il passaggio di quest'anno è solo il primo di altri cinque che si ripeteranno nel corso dei prossimi 12 anni. Nel 2018, 2003 SD220 passerà molto vicino alla Terra, a circa 0.019 UA. Grazie a questa vicinanza la Nasa sarà in grado di ottenere una stima della massa dell'oggetto, "un'informazione di inestimabile valore per comprendere densità e struttura interna" dell'asteroide.
    '''
},
{
    "_id" : "http://www.repubblica.it/scienze/2015/11/26/news/l_ultimo_urlo_della_stella_divorata_dal_buco_nero-128223087/", 
    "title" : "L'ultimo urlo della stella divorata dal buco nero", 
    "subTitle" : "È un gigantesco getto di materia lanciato nello spazio alla velocità della luce ed è stato osservato in diretta da un gruppo di scienziati della John Hopkins University" ,
    "data" : "26-11-2015" ,
    "evaluation":  3 ,
    "body" : '''UN ENORME SOS, un urlo gigantesco: è l'ultima richiesta di aiuto di una stella prima di venire ingoiata per sempre all'interno di un buco nero. Quello che assomiglia a un grido disperato nella realtà è un getto di materia lanciato nello spazio quasi alla velocità della luce nell'ultimo momento di vita della stella ormai senza speranza. Ad osservare in diretta la fine drammatica di questo astro, grande come il nostro Sole, è stato il gruppo internazione coordinato da Sjoert van Velzen, della John Hopkins University. La scoperta è descritta sulla rivista Science.
Eventi come questi sono "estremamente rari", commenta van Velzen. "È la prima volta", aggiunge, "che riusciamo a vedere tutto il processo, dalla distruzione stellare alla fuoriuscita di un getto di materia: tutto questo in diretta mentre accadeva, nell'arco di diversi mesi".
I buchi neri sono aree dello spazio così dense, che la loro forza gravitazionale irresistibile blocca la fuga di materia, gas e luce, rendendoli invisibili e creando l'effetto di un vuoto. Le teorie avevano previsto da tempo che quando un buco nero è costretto a 'ingoiare' una grande quantità di gas, può lasciar sfuggire una miscela di ioni ed elettroni. La conferma è arrivata soltanto adesso il fenomeno è stato osservato direttamente.
"La distruzione di una stella da parte di un buco nero è un evento meravigliosamente complicato",
conclude van Velzen, "e lontano dall'essere ancora pienamente compreso. Dalle nostre osservazioni abbiamo imparato che i flussi di detriti possono organizzarsi e produrre getti molto velocemente. Un tassello importante da aggiungere alla teoria di formazione di questi eventi" '''
},
{
    "_id" : "http://www.ilsole24ore.com/art/finanza-e-mercati/2015-11-17/solo-caso-come-l-indice-paura-ha-previsto-5-stragi-terroristiche-171534.shtml?uuid=ACAXjzbB&refresh_ce=1" , 
    "title" : "Solo un caso? Come l’indice della paura ha «previsto» 5 stragi terroristiche" , 
    "data" : "18-11-2015" ,
    "evaluation":  3 ,
    "body" : '''robabilmente è solo una coincidenza. Sta di fatto che - quando l’economista Stefano Fugazzi di Abc Economics ha provato a esaminare il comportamento dell’indice della paura (ossia il VIX, che misura la volatilità implicita di Wall Street) - si è trovato davanti a una sorpresa. Le stragi di Parigi di venerdì scorso, quella di Charlie Hebdo a gennaio, ma anche le bombe a Londra del 2005, quelle sui treni di Madrid del 2004 e l’attacco all’America dell’11 settembre 2001. In tutti questi casi l’indice Vix ha in qualche modo segnalato, nei cinque giorni precedenti alle stragi, un aumento della tensione sui mercati. Come è possibile?
Facciamo prima un passo indietro e spieghiamo bene che cos’è l’indice della paura. Attivo dal 1992, il Vix misura la volatilità dell’indice S&P 500 di Wall Street attraverso le opzioni (che sono strumenti derivati). In pratica, ci dice qual è la violenza dello spostamento dei prezzi che i mercati si attendono per il prossimo futuro. Più l’indice è alto, maggiore è la paura di un repentino sbalzo della Borsa. E dato che gli spostamenti più violenti e improvvisi sono quelli verso il basso (come dicono gli operatori, in Borsa “si sale sulle scale e si scende in ascensore”), di norma se il Vix sale i mercati si muovono nervosamente in discesa. Ecco perché si chiama “indice della paura”.
Torniamo all’analisi di Abc Economics. Come si può vedere nei grafici sull’indice Vix, relativi ai cinque più clamorosi attentati degli ultimi 15 anni nel mondo occidentale, possiamo notare due cose. Innanzitutto, nelle cinque sedute di Borsa precedenti gli attacchi, l’indice della paura è salito. Non solo e non tanto nel giorno dell’attentato, quanto nella media di quelli precedenti. Secondo elemento comune è il fatto che il giorno dopo gli attentati il Vix ha sempre chiuso a un livello inferiore a quello della seduta precedente. Con una sola eccezione: gli attacchi dell’11 settembre 2001, ma in quel caso gli attentati avvennero prima dell’apertura di Wall Street, che poi restò chiusa fino al 17 settembre. Il Chicago Stock Exchange, dove viene scambiato il Vix, in quell’occasione nemmenò iniziò le contrattazioni, quindi il caso specifico dell’11 settembre è atipico.
Coincidenze curiose. Molto probabilmente si tratta solo del caso: la salita del Vix nei giorni precedenti gli attentati dovrebbe essere stata provocata da altre ragioni, più squisitamente economico-finanziarie. Però è vero che il fenomeno si è ripetuto con una certa regolarità. E con un meccanismo che appare quello classico della Borsa: “buy the rumour, sell the news” (ossia compra sulle voci e vendi sulla notizia). Quasi sicuramente si tratta solo di coincidenze. Perché ogni altra ipotesi sarebbe terribile. '''
},
{
    "_id" : "http://www.huffingtonpost.it/2015/11/18/professore-nigeriano-matematica-risolve_n_8589076.html", 
    "title" : "Opeyemi Enoch, professore nigeriano, avrebbe risolto un problema di matematica irrisolto da 156 anni. In palio un milione di dollari (TWEET)", 
    "data" : "1-11-215" ,
    "evaluation":  4 ,
    "body" : ''' Hanno provato a risolverlo le menti più geniali della matematica, senza successo. Finalmente, dopo 156 anni, un professore nigeriano sarebbe riuscito a dimostrare l'ipotesi di Riemann, uno dei problemi più affascinanti e "irrisolvibili" proposti dal tedesco David Hilbert. Orgoglio e soddisfazione, ma non solo: se venisse accertata la sua soluzione come esatta dal Clay Mathematics Institute, Opeyemi Enoch (questo il nome dell'insegnante) potrebbe ricevere un premio di un milione di dollari. Il merito, secondo il professore, è tutto dei suoi alunni: "L'ho fatto perché loro hanno sempre creduto che sarei stato io a risolvere il quesito, non per i soldi".
Formulata nel 1859 da Bernhard Riemann, l'ipotesi è strettamente connessa ad i numeri primi ed essendo i numeri primi i "mattoncini" della matematica è stata fino ad oggi uno dei problemi centrali della matematica stessa. Per trovare una spiegazione, il professore Enoch, insegnante alla Federal University della città di Oye Ekiti, ha dovuto buttare giù molte delle convinzioni e dei luoghi comuni delle passate generazioni di geni della matematica.
Rimboccandosi le maniche e mosso da una grande passione, è arrivato a quella che sembrerebbe la soluzione definitiva e che potrebbe valergli il premio messo in palio dal Clay Mathematics Institute, che dal 2000 offre riconoscimenti a chi si dimostra in grado di risolvere storici problemi di matematica.
Il professor Enoch ha illustrato la sua scoperta all'International Conference on Mathematics and Computer Science e ha rivelato come le soluzioni che ha trovato possano trovare impieghi nella crittografia e nella scienza quantistica. Nonostante l'importanza della sua impresa, l'insegnante ha raccontato di essere stato preso di mira dalle critiche: "Se questo uomo è riuscito a risolvere il problema di Riemann, perché non trova una soluzione anche ai problemi della Nigeria?", è una delle domande che si è sentito porre. Ma anche a questo Enoch ha pensato: già prima di vincere il premio, aveva messo a punto un prototipo di silo per gli agricoltori poveri e sta lavorando per trovare il metodo migliore per proteggere gli oleodotti dagli atti di vandalismo. Come se non bastasse, poi, starebbe cercando di risolvere, con un approccio matematico, il problema del cambiamento climatico.
'''
},

{
    "_id" : "http://www.lanazione.it/umbria/marco-ciaccini-1.1474186", 
    "title" : "Un motore potente come il diesel che va a metano: Le Iene raccontano l'invenzione di Marco", 
    "data" : "11-11-2015",
    "evaluation":  4 ,
    "body" : ''' Il motore che azzera tutti i problemi da traffico? Potrebbe essere quello di un ragazzo di Calzolaro, in provincia di Perugia. Qui c'è Marco Ciaccini, che ha chiamato gli inviati di Italia Uno per promuovere quella che ha subito definito un motore rivoluzionario. Un'auto consumata dai chilometri e pagata cinquecento euro è stata sistemata per circa due anni, fino al risultato finale. Un comune motore diesel che secondo Marco può davvero cambiare le sorti dell'auto. Grazie a una serie di modifiche tecniche il motore può andare o completamente a gasolio, o completamente a metano o completamente a Gpl. Il ragazzo ha messo nel motore Diesel delle candele di accensione che riescono a dare al motore la doppia funzione. Le Iene hanno fatto il test. Ovvia la differenza di inquinamento tra il Diesel (che inquina 1.5) e il metano (0.1). Una invenzione che porta Calzolaro alla ribalta nazionale. La Iena Mauro Casciari prova l'auto. La macchina va a 110 all'ora ma non si "imballa" come accade alle vetture a metano. "Ha la potenza del Diesel ma non inquina allo stesso modo". Per fare 100 km si sono spesi due euro e mezzo, contro gli otto euro del motore diesel normale. Addirittura con la benzina verde si sarebbero spesi 13 euro. L'ingegner Daniele Toffanelli, soggetto "terzo" sentito dalle Iene, ha confermato che l'invenzione è valida. E il ragazzo ha registrato in 133 Paesi del mondo il brevetto. Che Marco riesca davvero, partendo da un paesino, a rivoluzionare il mondo dell'auto. Il futuro ce lo dirà. '''
},
{
    "_id" : "http://www.huffingtonpost.it/2015/11/10/piramidi_n_8520580.html", 
    "title" : "Piramidi, egittologi in fermento: Scansioni termiche rivelano impressionanti anomalie. Forse scoperti passaggi segreti", 
    "data" : "10-11-2015",
    "evaluation": 2  ,
    "body" : '''Un nuovo affascinante mistero avvolge le piramidi d’Egitto. Questa volta si tratta di alcune anomalie termiche riscontrate sulla superficie di quattro piramidi del Cairo, tra cui quella di Cheope. Gli egittologi sono in fermento di fronte alle prime analisi termiche effettuate dalla missione "Scan Pyramids", lanciata il 25 ottobre scorso da un team di ricercatori egiziani, francesi, giapponesi e canadesi, che aprono la strada a una molteplicità di possibili interpretazioni.
Grazie a una tecnologia agli infrarossi hi-tech e a scanner ultra sofisticati, i ricercatori hanno potuto sondare le piramidi di Cheope e Chefren, che con quella di Micerino si ergono nella piana di Giza, e le due di Dahchur, a sud del Cairo.
Gli scienziati hanno notato una quantità di “anomalie termiche” su tutti i monumenti esaminati ma una di queste, soprattutto, è considerata particolarmente impressionante. È stata riscontrata sulla superficie della piramide di Cheope, vicino al suolo. "Questa zona segnala una temperatura diversa”, ha spiegato il ministro alle Antichità egiziano, Mamdouh Eldamaty. “Che cosa potrebbe celarsi all'interno di questo tempio funerario? È una domanda che intriga tutti gli egittologi, soprattutto quelli interessati all'architettura egizia", ha sottolineato Eldamaty .
La temperatura della superficie della piramide è più o meno omogenea, a parte questi tre blocchi su cui la temperatura è di 6 gradi superiore a tutto il resto. Lo si può notare osservando le immagini delle telecamere termiche, dove appaiono dei colori caldi mentre il resto del monumento è caratterizzato da tinte fredde che spaziano dal blu al magenta.
In corrispondenza dei blocchi più caldi è stato notato qualcosa di simile a un piccolo passaggio nel terreno sul lato orientale della piramide. Le ragioni di queste anomalie restano ancora sconosciute ma già gli incauti favoleggiano di camere segrete all'interno delle quali potrebbe celarsi una tecnologia avanzatissima quanto misteriosa. Il governo egiziano definisce la scoperta ''un mistero'' e invita tutti gli egittologi a collaborare. Il progetto di ricerca continuerà fino alla fine del 2016 e rappresenta un nuovo tentativo per cercare di chiarire il segreto che ancora circonda, da oltre 4.000 anni, la costruzione delle piramidi. '''
},
{
    "_id" : "http://www.ilmessaggero.it/primopiano/esteri/mare_sommerge_citta_mondo_paura_studio_choc_foto-1350255.html", 
    "title" : "Il mare sommergerà le città di tutto il mondo: lo studio choc che spaventa il pianeta", 
    "subTitle" : "Presto il mare sommergerà gran parte dele città di tutto il mondo. La causa? Il riscaldamento terrestre che, con l'aumento delle temperature, scioglierà i ghiacci scaraventando tonnellate d'acqua sulle coste del nostro Pianeta. " ,
    "data" : "11-11-2015",
    "evaluation": 2  ,
    "body" : '''A dirlo è uno studio choc realizzato dall'organizzazione americana Climate Control che ha provveduto a diffondere motivazioni scientifiche a corredo di questa tesi. Le emissioni globali di gas serra - spiegano - potrebbero portare a enormi aumenti del livello delle acque, e alla conseguente scomparsa di molte delle coste di alcune tra le città più belle e importanti al mondo. Da New York a Londra, da Roma a Napoli fino a Shangai, Mumbai e tante altre.
Un dramma planetario evitabile? Forse sì se decide di affrontare con massima urgenza e serietà la problematica del contenimento drastico della CO2 liberata nell’atmosfera dall’attività umana. Perché i responsabili siamo noi che con l'inquinamento stiamo condizionando l'evoluzione della natura. Entro fine secolo infatti la temperatura globale potrebbe aumentare di 4°C provocando ciò che è mostrato nella gallery fotografica in solo alcuni degli esempi proposti. Un aumento che porterebbe a disastri meteo di portata catastrofiche e cambiamenti climatici impossibili da gestire mettendo a rischio, secondo una stima, dalle 400 alle 700 milioni di persone.
Riguardo l'Italia il pericolo riguarderebbe poco meno di 5 milioni d abitanti, spazzando via città come Venezia e Napoli e avvicinandosi molto a Roma con l'acqua che pian piano, partendo da Fiumicino, si farebbe avanti nell'entroterra. Secondo gli esperti evitare l'innalzamente delle temperatura è quasi impossibile ormai. Si tenta di contenere questo aumento intorno a 1,5°C in modo da garantire una speranza al popolo riducendo di conseguenza l'innalzamento del livello del mare. '''
},
{
    "_id" : "http://www.perdavvero.com/telefono/", 
    "title" : "Applica un pezzo di scotch sul telefono e poi lo colora. Quello che vede dopo è disgustoso.", 
    "data" : "01-04-2016",
    "evaluation":  4 ,
    "body" : ''' Sempre più frequentemente si parla di epidemie infettive o virus che portano influenza o malattie gastrointestinali. Nella maggior parte dei casi, la scarsa igiene e la presenza di batteri sulle superfici, anche dei bagni, ne sono la causa. Ma il problema è che queste minacce microscopiche non possono essere viste ad occhio nudo. Anche se… il telefono può portare alla luce la verità. Letteralmente...
Ecco cosa serve:
Scotch
Un pennarello indelebile blu
Un marcatore permanente viola
Un telefono cellulare con il flash e la funzione di torcia
Davvero allarmante ciò che viene alla luce, non trovate? Io voglio adottare questo piccolo trucco perché voglio essere più consapevole.
'''
},
{
    "_id" : "http://www.tomshw.it/news/scimmie-su-marte-la-russia-addestra-macachi-per-l-esplorazione-spaziale-71370", 
    "title" : "Scimmie su Marte, la Russia addestra macachi per l'esplorazione spaziale", 
    "subTitle" : "Quattro macachi Rhesus stanno seguendo un programma di addestramento russo in vista di un viaggio su Marte. Forse partiranno nel 2017." ,
    "data" : "28-10-2015",
    "evaluation": 2  ,
    "body" : '''Gli scienziati russi stanno addestrando 4 macachi Rhesus per una missione su Marte. Al momento non è chiaro se si tratterà di una missione di sola andata, quello che invece è certo è che i primati stanno seguendo un addestramento di 3 ore al giorno propedeutico a imparare a usare un joystick e risolvere puzzle.
L'addestramento, guidato da Inessa Kozlovskaya, in questa fase prevede l'uso di un joystick per centrare e colpire un bersaglio evidenziato da un cursore. Il successo viene ricompensato con un sorso di succo di frutta. Una volta che avranno dimestichezza con questo compito i macachi saranno addestrati per risolvere semplici puzzle, e al termine della formazione dovrebbero essere in grado di completare autonomamente un programma giornaliero di attività.
Gli esperti dell'Accademia Russa delle Scienze auspicano di poter completare l'addestramento e di spedire le scimmie su Marte entro il 2017.  Tutti e quattro i macachi Rhesus coinvolti in questo studio sono stati scelti per le loro capacità cognitive e la velocità di apprendimento.
A leggere questa notizia viene in mente la cagnolina Laika, che fu spedita nello Spazio il 3 novembre 1957 e non sopravvisse alla missione. Il suo sacrificio sollevò molte proteste in tutto il mondo, e al termine della Guerra Fredda Oleg Gazenko, che fu responsabile della missione, ammise che il lancio di Laika fu un sacrificio inutile. Laika non fu né il primo né l'ultimo cane spedito nello Spazio.
Nonostante questo gli animali hanno svolto spesso il ruolo di pionieri spaziali, e oltre ai cani le scimmie furono protagoniste di molti esperimenti. Il primo primate di andare nello Spazio fu Alberto I nel giugno del 1948, a bordo di un razzo V-2 degli Stati Uniti: soffocò e morì durante il volo.
Un anno dopo, quasi lo stesso giorno, un secondo V-2 trasportò Alberto II ad un'altitudine di 133 chilometri: il razzo rientrò sulla Terra ma Alberto II morì nell'impatto. Nel dicembre dello stesso anno, Alberto IV fu inviato nello Spazio collegato a macchine per il monitoraggio della sua salute. Come Alberto II non subì effetti negativi dal volo, ma non sopravvisse al rientro.
Ci vollero altri due anni prima che un'altra scimmia, Yorick, intraprendesse un viaggio nello Spazio e sopravvisse. Poi fu la volta di Ham the Astrochimp nel gennaio del 1961: le sue attività vitali furono monitorate e fu in grado di azionare delle leve durante il volo.
Saranno le ennesime cavie animali a riaprire la corsa allo Spazio e a portare l'uomo su Marte? La Russia sembra non scartare l'idea, almeno stando a questa notizia. Dall'altra parte dell'Oceano la NASA sembra preferire la scommessa tecnologica e spedisce rover e sonde a iosa sul Pianeta Rosso per tastare il terreno. Voi cosa ne pensate?    '''
},
{
    "_id" : "http://www.dionidream.com/autismo-vaccini-feti-umani-abortiti/", 
    "title" : "Autismo legato ai vaccini derivati da feti umani abortiti", 
    "subTitle" : "Helen Ratajczak, ricercatrice della Boehringer Ingelheim Pharmaceuticals, ha mostrato come l’utilizzo di vaccini coltivati in tessuti umani sia correlabile all’autismo. Infatti il picco di autismo si raggiunse nel 1995 quando il vaccino della varicella fu allevato in tessuti fetali umani. L’articolo è stato pubblicato su Pubmed." ,
    "data" : "28-8-2015",
    "evaluation": 4  ,
    "body" : '''La CBS News ha riportato: “L’articolo sul Journal of Immunotoxicology è intitolato Theoretical aspects of autism: causes–a review. L’autore è Helen Ratajczak, a sorpresa lei stessa un ex scienziato senior presso una ditta farmaceutica. Ratajczak ha fatto ciò che nessun’altro si era mai preso la briga di fare: ha rivisto tutto il corpo scientifico pubblicato sull’autismo a partire dal 1943. 
La Dr. Ratajczak riferisce che quando i produttori di vaccini hanno dovuto eliminare il thimerosal dai vaccini (con l’eccezione dei vaccini contro l’influenza che ancora contengono thimerosal), hanno cominciato a produrre alcuni vaccini utilizzando tessuti umani.
L’utilizzo di tessuti umani secondo la Ratajczak riguarda attualmente 23 vaccini. Nel suo studio ha discusso l’aumento di incidenza dell’autismo in corrispondenza con l’introduzione di DNA umano nel vaccino MMR, e suggerisce che le due cose potrebbero essere collegate.”
Le pagine della revisione contengono un dettaglio che difficilmente poteva passare inosservato, cinque parole che rivelano uno dei segreti più shoccanti di Big Pharma, delle aziende farmaceutiche cioè: “…allevato in tessuti fetali umani”
A pagina 70 si legge: “Un aumento aggiuntivo del picco di autismo si raggiunse nel 1995 quando il vaccino della varicella fu allevato in tessuti fetali umani”.
La maggior parte di noi è del tutto ignara che le cellule di cultura umana usate per allevare i virus dei vaccini derivano da feti abortivi da decenni ormai e chi li produce è ben felice che il pubblico continui ad ignorarlo, perché sa che questo non potrebbe essere accettato dalla gente sia per le ignote conseguenze per la nostra salute che per il credo religioso di molti.
Perché il DNA umano potrebbe potenzialmente causare danni al cervello? Così Ratajczak ha spiegato: “Perché il DNA è umano e destinatari sono gli esseri umani, c’è omologo recombinaltion tiniker. Quel DNA è incorporato nel DNA ospite. Ora è cambiato, alterato e questo corpo lo uccide. Dov’è espresso maggiormente? I neuroni del cervello. Ora hai questo corpo che uccide le cellule del cervello ed è un’infiammazione che va avanti. Non si ferma, continua attraverso la vita di quella persona.”
Il vaccino contro la varicella non è l’unico prodotto in questo modo e, secondo il Sound Choice Pharmaceutical Institute (SCPI), i seguenti 24 vaccini sono prodotti usando cellule provenienti da feti abortivi e/o contenenti DNA, proteine, o frammenti cellulari di colture di cellule coltivate derivate da feti umani abortivi:
Polio PolioVax, Pentacel, DT Polio Absorbed, Quadracel (Sanofi)
Measles, Mumps, Rubella MMR II, Meruvax II, MRVax, Biovax, ProQuad, MMR-V (Merck)
Priorix, Erolalix (GlaxoSmithKline)
Varicella (Chickenpox and Shingles) Varivax, ProQuad, MMR-V, Zostavax (Merck)
Varilix (GlaxoSmithKline)
Hepatitis A Vaqta (Merck)
Havrix, Twinrix (GlaxoSmithKine)
Avaxim, Vivaxim (Sanofi)
Epaxal (Crucell/Berna)
Rabies Imovax (Sanofi)
La National Network for Immunization Information (NNII) ha pubblicato sul suo sito un articolo che spiega le motivazione dell’uso di feti umani abortivi per la produzione dei vaccini e fa una piccola storia di questa “tecnica”. L’articolo afferma che i feti in questione non sono stati abortiti con lo scopo di effettuare ricerca e produrre vaccini, né sono stati i biologi a praticare essi stessi gli aborti e si dilunga nello spiegare come sia difficile e complicato produrre questi farmaci:
“La produzione di farmaci biologici (per esempio vaccini, anticorpi, ecc) è molto più difficile di quanto sia quella di farmaci chimici (ad esempio, la penicillina o aspirina). Inoltre, alcuni vaccini sono più complessi da realizzare rispetto ad altri. I batteri che vanno in vaccini batterici possono essere coltivati in semplici colture di laboratorio, ma la crescita del virus richiede cellule viventi.
I virus non possono riprodursi da soli. Essi richiedono un ospite vivente in cui crescere, come gli embrioni di pollo, e le cellule da animali che sono cresciuti in coltura. Le aziende che producono i vaccini attualmente hanno poche opzioni per la cultura virale, a causa di valide ragioni farmaceutiche e per motivi di sicurezza. Per esempio, nel caso della varicella, il virus non cresce bene nella maggior parte delle cellule se non in quelle umane. Inoltre, le cellule umane sono da preferire perché le cellule derivate da organi di animali a volte possono portare virus animali che potrebbero danneggiare le persone“.
E’ assolutamente possibile preparare vaccini virali senza l’uso di cellule umane e animali, come prova l’attuale vaccino contro l’epatite B che usa cellule di lieviti. Perchè quindi i produttori non lo fanno, optando invece per metodi più controversi?
Anche il sito ABC News discute della questione, essendo un problema sia dal punto di vista della salute che da quello etico.
Concludo con le parole del Dr. Mercola:”Ci poniamo quindi alcune semplici domande: possibile che tutto questo non preoccupi e non interessi nessuno? Perché un silenzio così sordo su un tema così grave? Possibile che abbia così poca importanza sapere quale tipo di proteine umane, DNA, sostanze chimiche e tossine possano essere contenute in prodotti che iniettiamo nei nostri neonati, nei nostri bambini…. quanto di più sacro dovremmo avere? Può il profitto davvero giustificare anche questo?” '''
},
{
    "_id" : "http://www.ilgiornale.it/news/mondo/lastronauta-dellapollo-14-grazie-agli-alieni-abbiamo-evitato-1161289.html", 
    "title" : "L'astronauta dell'Apollo 14: Grazie agli alieni abbiamo evitato la guerra nucleare", 
    "subTitle" : "Edgar Mitchell: Gli alieni hanno contribuito a evitare che la Guerra fredda tra Stati Uniti e Unione Sovietica potesse degenerare in una guerra nucleare" ,
    "data" : "18-08-2015",
    "evaluation":  4 ,
    "body" : '''Chiamiamoli salvatori delle patrie. Sì, perché è merito degli alieni se la guerra fredda tra Stati Uniti e Russia non si è tramutata in una guerra nucleare.
    Parola di Edgar Mitchell, ex pilota della storica missione Apollo 14. La tesi del sesto uomo che mise piede sulla Luna può sembrare bizzarra, alla stregua di pellicole di fantascienza come “E.T.”. Eppure, dichiara Mitchell al Mirror, “gli extraterrestri volevano sapere quali fossero le nostre potenzialità militari e sono certo che stavano tentando di fermare la guerra e di aiutarci a vivere in pace”. L’astronauta racconta che alcuni militari videro strani oggetti sorvolare le basi missilistiche americane e la base Nasa White Sands in New Mexico, dove nel 1945 fu eseguito il primo esperimento atomico.
Mitchell è cresciuto proprio in New Mexico, vicino al Roswell site dove nel 1947 precipitò quello che per molti poteva essere un Ufo, e sostiene di aver ricevuto delle informazioni molto interessanti dall'Air Force. "Mi hanno detto che era frequente avvistare Ufo nel cielo e che spesso questi disabilitavano i loro missili. Altri ufficiali delle basi sul Pacifico mi hanno raccontato che i missili sono stati più volte colpiti con proiettili lanciati da oggetti alieni”. Insomma, per Mitchell gli extraterrestri esistono e dovremmo ringraziarli per averci risparmiato un conflitto devastante. Torneranno dalle nostre parti per fermare tragedie e invasioni in corso?'''
},
{
    "_id" : "http://www.huffingtonpost.it/johann-hari/la-piu-probabile-causa-dipendenza_b_6537964.html", 
    "title" : "La più probabile causa della dipendenza è stata scoperta - e non è ciò che credete", 
    "data" : "24-01-2015",
    "evaluation":  2 ,
    "body" : ''' Sono ormai passati cent'anni da quando le droghe sono state proibite per la prima volta - e nel corso di questo lungo secolo di guerra alla droga, i nostri insegnanti e i governi ci hanno raccontato una storia sulla dipendenza. Una storia tanto radicata nelle nostre menti che la diamo per assodata. Pare ovvia. Sembra palesemente vera. Lo credevo anch'io, fino a quando tre anni e mezzo fa non mi sono imbarcato in un viaggio di 30mila miglia per lavorare al mio nuovo libro, Chasing The Scream: The First And Last Days of the War on Drugs, alla scoperta di ciò che c'è veramente dietro alla guerra alla droga. Ciò che ho imparato lungo la mia strada è che quasi tutto ciò che c'è stato raccontato sulla dipendenza è sbagliato - e che di storia ne esiste un'altra, molto diversa, che aspetta ancora d'esser raccontata, se solo saremo disposti ad ascoltarla.
Se faremo nostra questa nuova storia ci toccherà cambiare non solo la guerra alla droga. Dovremo cambiare noi stessi.
Ciò che ho imparato l'ho appreso da un mucchio di persone straordinariamente diverse che ho incontrato lungo i miei viaggi. Dagli amici ancora vivi di Billie Holiday, da cui ho scoperto che il fondatore della guerra alla droga l'aveva perseguitata, contribuendo alla sua morte. Da un dottore ebreo portato di nascosto via dal ghetto di Budapest quand'era piccolo, per poi scoprire da adulto i segreti della dipendenza. Da un trafficante transessuale di crack a Brooklyn, concepito quando la madre, dipendente dal crack, fu stuprata dal padre, un agente della polizia di New York. Da un uomo che è stato relegato in fondo a un pozzo per due anni da una dittatura dedita alla tortura, per poi riemergerne e finire un giorno col venire eletto presidente dell'Uruguay, segnando così gli ultimi giorni della guerra alla droga.
Avevo un motivo piuttosto personale per andare alla ricerca di tutte queste risposte. Uno dei miei primi ricordi da piccolo è stato quella di provare a svegliare un mio parente, senza riuscirci. Da allora mi sono rigirato in testa uno dei misteri essenziali della dipendenza - cos'è che fa sì che ci sia gente che diventa tanto ossessionata da una droga, o da un determinato comportamento, da non riuscire più a fermarsi? Come si può fare per aiutare quella gente a tornare da noi? Crescendo, un altro dei miei parenti più stretti sviluppò una dipendenza da cocaina, e io iniziai un rapporto con una persona dipendente dall'eroina. In un certo senso la dipendenza per me era di casa.
Se tempo fa mi aveste chiesto quale fosse l'origine della dipendenza dalla droga, vi avrei guardato come degli idioti, e vi avrei detto: "Beh, la droga, no?". Non era difficile da capire. Ero convinto di averlo esperito in prima persona. Siamo tutti in grado di spiegarlo. Supponiamo che voi e me, insieme ai prossimi venti passanti, stabilissimo di somministrarci per venti giorni di fila una droga veramente potente. Siccome queste droghe sono dotate di forti ganci chimici, se il ventunesimo giorno poi smettessimo, i nostri corpi finirebbero per bramare quella sostanza. Una bramosia feroce. Saremmo dunque diventati dipendenti da essa. Ecco che cosa significa 'dipendenza'.
La teoria è stata in parte codificata grazie agli esperimenti compiuti sui topi - entrati nella psiche collettiva americana negli anni '80 grazie a una nota campagna pubblicitaria di Partnership for a Drug-Free America. Potreste ricordarla. L'esperimento è piuttosto semplice. Mettete un topo in gabbia, da solo, con due bottiglie d'acqua. Una contiene solo acqua. L'altra anche eroina o cocaina. Quasi ogni singola volta in cui l'esperimento viene ripetuto, il topo finirà ossessionato dall'acqua drogata, e tornerà a chiederne ancora fino al momento in cui morirà.
La pubblicità lo spiegava così: "C'è solo una droga in grado d'indurre tanta dipendenza, e nove topi di laboratorio su dieci ne faranno uso. Ancora. E ancora. Fino alla morte. Si chiama cocaina. E a voi può fare lo stesso".
Tuttavia negli anni '70 un docente di psicologia a Vancouver di nome Bruce Alexander notò qualcosa di strano in questo esperimento. Il topo viene messo in una gabbia da solo. Non ha altro da fare che somministrarsi la droga. Che succederebbe allora, si chiese, se lo impostassimo diversamente? Così il professor Alexander costruì un 'parco topi'. Una gabbia di lusso all'interno della quale i topi avrebbero avuto a disposizione delle palline colorate, il miglior cibo per roditori, delle gallerie nelle quali zampettare e tanti amici: tutto ciò a cui un topo metropolitano avrebbe potuto aspirare. Che cosa sarebbe accaduto in quel caso, si chiedeva Alexander?
Nel 'parco topi' tutti ovviamente finivano per assaggiare l'acqua di entrambe le bottiglie, non sapendo che cosa ci fosse dentro. Ma ciò che successe in seguito fu sorprendente.
Ai topi che facevano una bella vita l'acqua drogata non piaceva. Perlopiù la evitavano, consumandone meno di un quarto rispetto ai topi isolati. Nessuno di loro morì. E mentre tutti i topi tenuti soli e infelici ne facevano uso pesante, ciò non accadeva ad alcuno di quelli immersi in un ambiente felice.
All'inizio pensai che si trattasse soltanto di una stranezza dei topi, finchè non scoprii che - nello stesso periodo dell'esperimento del 'parco topi' - c'era stato il suo equivalente umano. Si chiamava guerra in Vietnam. La rivista Time scriveva che fra i soldati americani l'uso di eroina era "comune quanto quello della gomma da masticare", e che ce n'erano delle prove concrete: stando a una ricerca pubblicata negli Archives of General Psychiatry circa il 20 per cento dei soldati americani in quel Paese erano diventati dipendenti dall'eroina. In tanti se ne sentirono comprensibilmente terrorizzati; convinti che alla fine della guerra in patria sarebbe rientrato un enorme numero di tossicodipendenti.
La verità è che circa il 95 per cento dei soldati che avevano sviluppato quella dipendenza - stando alla medesima ricerca - in seguito semplicemente non si drogarono più. In pochi furono costretti alla riabilitazione. Il fatto è che erano passati da una gabbia terrificante a una piacevole, per cui smisero di anelare alla droga.
Il professor Alexander ritiene che questa scoperta contesti in modo profondo sia il punto di vista destrorso, per cui la dipendenza non è che una questione 'immorale' generata dagli eccessi dell'edonismo festaiolo, sia quello liberal per cui la dipendenza è quel male che attecchisce all'interno di un cervello alterato dalle sostanze chimiche. Anzi, argomenta, la dipendenza è una forma d'adattamento. Non sei tu. È la tua gabbia.
Dopo la prima fase del 'parco topi' il professor Alexander portò avanti il test. Tornò a ripetere gli esperimenti originari, quelli in cui i topi venivano lasciati da soli e facevano compulsivamente uso della droga. Lasciò che ne facessero uso per cinquantasette giorni - una quantità di tempo sufficiente ad agganciarli. Poi li portò fuori dall'isolamento, collocandoli all'interno del 'parco topi'. Voleva capire se, una volta sviluppata una dipendenza, il cervello risultasse talmente alterato da non potersi più riprendere. Se le droghe in effetti s'impossessavano di te. Ciò che accadde risultò - ancora una volta - stupefacente. I topi mostravano qualche problema d'astinenza, ma smettevano presto di farne uso intensivo, tornando a vivere una vita normale. La gabbia buona li aveva salvati (i riferimenti precisi a tutte le ricerche a cui faccio riferimento sono nel libro).
Quando per la prima volta incappai in tutto questo rimasi perplesso. Che senso aveva? Questa nuova teoria criticava in maniera talmente radicale ciò che ci era stato detto che sembrava non potesse esser vera. Ma più scienziati intervistavo, più consultavo le loro ricerche, più scoprivo cose che non sembravano aver alcun senso - a meno che non si prendesse in considerazione questo nuovo approccio.
Ecco l'esempio di un esperimento che si sta conducendo, e che un giorno potrebbe riguardarvi direttamente. Se oggi v'investissero e subiste una frattura al bacino, vi verrebbe probabilmente somministrata la diamorfina, nome medico dell'eroina. Nell'ospedale in cui vi troverete ci sarà tanta altra gente a cui viene somministrata l'eroina per lunghi periodi, per attenuarne il dolore. L'eroina che vi darà il medico sarà molto più pura e potente di quella adoperata dai tossici per strada, costretti a comprarla da spacciatori che la tagliano. Ragion per cui, se la vecchia teoria della dipendenza fosse valida - sono le droghe a causarla, perché fanno sì che il tuo corpo ne senta il bisogno - la conseguenza sarebbe ovvia. Un mucchio di gente dovrebbe lasciare l'ospedale per finire alla ricerca di una dose per strada, assecondando la dipendenza che avrebbero sviluppato.
Ma ecco la cosa strana: questo praticamente non succede mai. Come il medico canadese Gabor Mate mi ha spiegato per la prima volta, coloro che ne fanno uso medico poi semplicemente smettono, pur essendo stata loro somministrata per mesi. La medesima droga, fruita per la medesima quantità di tempo, trasforma chi ne fa uso per strada in tossici disperati, lasciando immutati i pazienti d'ospedale.
Se siete ancora convinti - come anch'io un tempo - che la dipendenza sia causata dai ganci chimici, la cosa non avrà alcun senso. Ma se credete alla teoria di Bruce Alexander, tutto torna. Il tossico per strada è un po' come i topi della prima gabbia, isolato, solo, con un'unica fonte di consolazione a portata di mano. Il paziente d'ospedale è come il topo della seconda gabbia. Si prepara a tornare a casa, a una vita in cui sarà circondato dalla gente che ama. La droga è la stessa, l'ambiente però è diverso.
Questo ci fornisce un'intuizione che va ben oltre il bisogno di comprendere i tossicodipendenti. Il professor Peter Cohen sostiene che gli esseri umani abbiano una profonda necessità di formare legami ed entrare in contatto gli uni con gli altri. È così che ci gratifichiamo. Se non siamo in grado di entrare in contatto con gli altri, entreremo in contatto con qualsiasi altra cosa - il suono di una roulette che gira, o l'ago di una siringa. Lui è convinto che dovremmo smettere del tutto di parlare di 'dipendenza', e chiamarla piuttosto 'legame'. Un eroinomane si lega all'eroina perché non è stato in grado di legare in modo altrettanto forte con nient'altro.
Ragion per cui il contrario della dipendenza non è la sobrietà. Ma il contatto umano.
Quando ho saputo tutto questo, ho scoperto di aver cominciato a convincermene, ma non sono comunque riuscito a liberarmi da un dubbio assillante. Tutti questi scienziati sono forse convinti che i ganci chimici non facciano alcuna differenza? Così me l'hanno spiegato - puoi diventare dipendente dal gioco d'azzardo, e nessuno penserà mai che t'inietti un mazzo di carte in vena. Per cui potrai avere il massimo della dipendenza, e nessun gancio chimico. Ho partecipato a un incontro dei giocatori d'azzardo anonimi di Las Vegas (col permesso di tutti i partecipanti, che sapevano di essere osservati) e mi sembravano chiaramente dipendenti, tanto quanto qualsiasi altro cocainomane o eroinomane io abbia mai incontrato. Eppure di ganci chimici sul tavolo da gioco non ce ne sono.
Di certo, però, ribattevo, le sostanze chimiche lo dovranno svolgere un qualche ruolo. Salta fuori che esiste un esperimento in grado di rispondere in termini molto precisi a questa domanda. L'ho scoperto leggendo il libro The Cult of Pharmacology, di Richard DeGrandpre.
Tutti concordano sul fatto che il fumo della sigaretta sia uno dei più grandi generatori di dipendenza. I ganci chimici del tabacco derivano da una droga al suo interno chiamata nicotina. Quando nei primi anni '90 sono stati sviluppati i cerotti alla nicotina ci fu un grande ottimismo - i fumatori di sigaretta avrebbero potuto godersi tutti gli amati ganci chimici senza le sporche (e letali) controindicazioni del fumo. Sarebbero stati liberi.
Ma la Direzione generale della sanità ha scoperto che appena il 17,7 per cento dei fumatori di sigarette sono in grado di mettere adoperando i cerotti alla nicotina. Ora, non è proprio roba da nulla. Se le sostanze chimiche rappresentano il 17,7 per cento della dipendenza, come si è dimostrato, si parla comunque di milioni di vite rovinate in tutto il mondo. Ciò che però si scopre, ancora una volta, è che la storia che ci è stata insegnata sui ganci chimici come Causa della Dipendenza, per quanto vera, non è che un frammento all'interno di un mosaico più vasto.
Le implicazioni per l'ormai centenaria guerra alla droga sono notevoli. Quest'enorme crociata - che come ho avuto modo di osservare uccide gente dai centri commerciali messicani alle strade di Liverpool - si fonda sulla convinzione che sia necessario eliminare fisicamente una vasta quantità di sostanze chimiche perchè s'impossessano dei cervelli della gente e ne causano la dipendenza. Ma se non sono le droghe a portare alla dipendenza - se anzi a causarla è quel senso di scollegamento dagli altri - tutto questo non ha alcun senso.
Ironicamente la guerra alla droga non fa che alimentare i macro fattori che portano alla dipendenza. Ad esempio mi sono recato in una prigione in Arizona - 'Tent City' - dove per punirli per l'uso di droga i detenuti vengono costretti per settimane e settimane all'interno di minuscole celle d'isolamento in pietra (le chiamano 'il Buco'). Cioè quanto di più vicino si possa arrivare a ricreare per gli uomini le gabbie che garantivano la dipendenza letale dei topi. E quando poi quei detenuti ne fuoriescono, la fedina penale impedirà loro di essere assunti - garantendone per sempre l'isolamento. L'ho visto accadere in diversi casi a persone che ho incontrato in giro per il mondo.
Esiste un'alternativa. Si può costruire un sistema concepito per aiutare i tossicomani a rientrare in contatto col mondo - lasciandosi la dipendenza alle spalle.
Non è teoria. Succede davvero. L'ho visto coi miei occhi. Quasi quindici anni fa il Portogallo aveva una delle situazioni peggiori di tutta Europa quanto a diffusione degli stupefacenti, con l'1 per cento della popolazione dipendente da eroina. Avevano provato con la guerra alla droga, e il problema non faceva che peggiorare. Così decisero di fare qualcosa di drasticamente diverso. Stabilirono di depenalizzare tutti gli stupefacenti, rinvestendo il denaro che prima spendevano per arresto e detenzione del tossicomane, e adoperandolo invece per rimetterlo in comunicazione - coi propri sentimenti e con la società più ampia. Il passo determinante è quello di assicurargli un'abitazione stabile e un posto di lavoro sociale così da offrirgli uno scopo nella vita, e una ragione per alzarsi dal letto. Li osservavo mentre venivano aiutati all'interno di ambulatori ricchi di calore umano e accoglienti, per imparare a tornare in contatto coi propri sentimenti, dopo anni di trauma e di silenzioso stordimento dovuto alle droghe.
Uno degli esempi di cui sono venuto a conoscenza è un gruppo di tossicodipendenti a cui è stato offerto un prestito per mettere in piedi una piccola azienda di traslochi. D'un tratto erano diventati un gruppo, legarono tutti fra loro, e con la società, e si fecero responsabili della cura dell'altro.
I primi risultati stanno arrivando. Una ricerca indipendente del British Journal of Criminology ha scoperto che dal momento della sua totale depenalizzazione le dipendenze sono crollate, e l'uso di stupefacenti da iniezione è diminuito del 50 per cento. Lasciatemelo ripetere: l'uso di stupefacenti da iniezione è diminuito del 50 per cento. Il risultato della depenalizzazione è stato un successo talmente chiaro che in pochi in Portogallo aspirano a tornare al vecchio sistema. Il primo oppositore della depenalizzazione, nel 2000, era stato Joao Figueira, il più importante poliziotto antidroga del Paese. All'epoca lanciava quel genere di avvertimenti che ci si aspetterebbe dal Daily Mail o da Fox News. Ma quando poi ci siamo incontrati a Lisbona, mi ha spiegato come le sue previsioni non si siano avverate, e come oggi lui speri che tutto il mondo segua l'esempio del Portogallo.
Tutto ciò non riguarda solo i tossicodipendenti a cui voglio bene. Riguarda tutti noi, perché ci costringe a pensare a noi stessi in maniera diversa. Gli esseri umani sono animali sociali. Abbiamo bisogno legare, di entrare in contatto e di amare. La frase più saggia del ventesimo secolo appartiene a E.M. Forster: "Mettetevi in contatto". Ma noi abbiamo creato un ambiente e una cultura che ci isolano da ogni forma di connessione, o che ce ne offrono solo la parodia generata da internet. La crescita delle dipendenze è il sintomo di un male profondo del modo in cui viviamo - volgendo costantemente lo sguardo all'ennesimo gadget luccicante da acquistare, piuttosto che agli esseri umani intorno a noi.
Lo scrittore George Monbiot l'ha chiamata "l'epoca della solitudine". Abbiamo creato società umane all'interno delle quali isolarsi da ogni legame è più facile che mai prima d'ora. Bruce Alexander - l'ideatore del 'parco topi' - mi ha spiegato come per troppo tempo non abbiamo fatto altro che parlare della riabilitazione dell'individuo dalla dipendenza. Ciò di cui abbiamo bisogno di parlare oggi è la riabilitazione sociale - un modo per riabilitare noi tutti, insieme, dal male dell'isolamento che ci sta avvolgendo come una spessa coltre di nebbia.
Ma queste nuove scoperte non rappresentano esclusivamente una sfida politica. Non sono solo le nostre menti che c'impongono di cambiare. Ma i nostri cuori.
Amare un tossicodipendente è davvero dura. Quando guardavo alle persone dipendenti a cui volevo bene, una volta avevo sempre la tentazione di seguire i consigli di reality show come Intervention - intimando a chi aveva una dipendenza di mettersi in riga, o allontanandolo. Il messaggio era che un tossicodipendente che non è in grado di smettere dovrebbe essere rifiutato. È la logica della guerra alla droga, interiorizzata nel privato. E invece, come ho avuto modo di capire, ciò non fa che peggiorare la loro condizione - e potresti finire per perdere del tutto la persona. Quando sono tornato a casa ero determinato a tenermi stretto più che mai le persone dipendenti che facevano parte della mia vita - facendo loro capire che il mio amore per loro è incondizionato, cioè indipendente dal fatto che smettano o che non ci riescano.
Quando sono tornato dal mio lungo viaggio ho guardato in faccia il mio ex-ragazzo, in crisi d'astinenza, che tremava sul letto degli ospiti, e ho pensato a lui in maniera diversa. Da un secolo intoniamo canti di guerra contro i tossicodipendenti. Asciugandogli la fronte mi è venuto in mente che forse quello che avremmo dovuto fare in tutto questo tempo sarebbe stato cantargli delle canzoni d'amore.
Questo blog è stato pubblicato originariamente su Huffington Post United States ed è stato tradotto dall'inglese all'italiano da Stefano Pitrelli.'''
},
{
    "_id" : "http://www.repubblica.it/ambiente/2015/08/04/foto/temperature_record_iran-120395042/", 
    "title" : "Temperature record in Iran, percepiti 70 gradi", 
    "subTitle" : "http://www.repubblica.it/ambiente/2015/08/04/foto/temperature_record_iran-120395042/1/#1" ,
    "data" : "04-082015",
    "evaluation": 4  ,
    "body" : '''Caldo record in Iran, dove la temperatura percepita ha sfiorato i 73 gradi nella località di Bandar Mahshahr, sul Golfo Persico. E’ quanto riferiscono i meteorologi che non prevedono miglioramenti per i prossimi giorni. Se confermata, si tratterebbe della temperatura più alta mai registrata nel mondo. Nella zona sud-ovest del Paese, l’umidità sprigionata dalle alte temperature delle acque si è andata ad aggiungere al calore soffocante della terra. Nel 2005, nel deserto di Lut, al confine con l’Afghanistan, era stata rilevata una temperatura di 70,7 gradi. Anche nel vicino Iraq in questi giorni si sono superati i 49 gradi costringendo le autorità locali a indire quattro giorni di festa e invitare i cittadini a rimanere al riparo dal sole. In Italia il valore più elevato della storia è stato registrato il 10 agosto 1999 a Catena Nuova, in provincia di Enna: 48,5 gradi'''
},
{
    "_id" : "http://www.kevideo.eu/gaffe-clamorosa-il-capitano-delliss-ammette-che-luomo-non-e-mai-andato-sulla-luna-video/", 
    "title" : "Gaffe clamorosa! Il capitano dell’ISS ammette che l’uomo non è mai andato sulla Luna? – VIDEO", 
    "data" : "26-07-2015",
    "evaluation": 4  ,
    "body" : '''Durante una intervista televisiva a fianco di Samantha Cristoforetti, il capitano della Stazione Spaziale Internazionale, Terry Virts, dichiara che l’uomo non è in grado di andare oltre l’orbita terrestre.
Su Wikipedia possiamo controllare che esistono 3 tipi di orbite terrestri: bassa, media e alta. Cito:
Orbita terrestre bassa, in cui si trova ad esempio la Stazione Spaziale Internazionale
Orbita terrestre media, in cui si trovano i satelliti dei sistemi di navigazione (GLONASS, Galileo e GPS).
Orbita terrestre alta (particolarmente ellittica)
L’Orbita terrestre bassa è compresa tra i 160 e 2.000 km.
L’Orbita terrestre media è compresa tra i 2.000 ed i 35.786 km.
L’Orbita terrestre alta è superiore ai 35.786 km.
La Luna si trova ad una distanza dalla Terra di 384.400 km, circa 10 volte l’orbita terrestre alta e 190 volte l’orbita terrestre bassa!
Com’è possibile che attualmente non si possa andare più lontano dell’orbita terrestre se mezzo secolo fa siamo andati sulla luna???
TESTO DEL VIDEO.
DOMANDA: Che cosa ci sarà dopo la Stazione Spaziale Internazionale? Una volta che la sua missione sarà completata, come garantiremo la presenza di umani nello spazio?
VIRTS: Ottima domanda! La NASA sta progettando di costruire un razzo chiamato SLS, un razzo per carichi pesanti che è molto più grande di quelli che abbiamo oggi. […]
Sarà in grado di lanciare la capsula Orion con uomini a bordo oltre che a veicoli per atterrare [sui pianeti], ed altri strumenti verso destinazioni al di là dell’orbita terrestre.
Attualmente siamo in grado di volare solo nell’orbita terrestre, più lontano di così non possiamo andare. Il nuovo sistema che stiamo costruendo ci permetterà di andare oltre, e speriamo che possa portare degli uomini ad esplorare il sistema solare. La LUNA, Marte, gli asteroidi, ci sono molte destinazioni che potremmo raggiungere e stiamo costruendo i vari pezzi che ci permetteranno un giorno di farlo.
'''
},
{
    "_id" : "http://www.greenme.it/vivere/salute-e-benessere/16914-preservativo-cambia-colore-infezioni", 
    "title" : "STEYE, IL PROFILATTICO CHE CAMBIA COLORE SE RILEVA INFEZIONI E MALATTIE", 
    "subTitle" : "Infezioni e malattie a trasmissione sessuale, ecco il preservativo in grado di rilevarle e di cambiare colore per avvisarci. Il profilattico intelligente è nato dall'idea di un gruppo di studenti che ha partecipato al premio britannico TeenTech Award." ,
    "data" : "24-06-2015",
    "evaluation": 2  ,
    "body" : '''L'obiettivo è di ridurre la diffusione delle malattie a trasmissione sessuale e l'aumento del tasso di infezione. Il profilattico intelligente si chiama Steye. Per realizzarlo gli studenti hanno avuto l'idea di sfruttare le molecole associate ai batteri e ai virus delle infezioni e delle malattie a trasmissione sessuale più comuni.
Quando queste molecole si legano ai virus o ai batteri, diventano fluorescenti ed ecco che così ci avvertono dell'infezione. E la reazione è visibile anche al buio. Il preservativo intelligente diventa di colori diversi a seconda della malattia o dell'infezione rilevata.
Ad esempio, Steye diventa giallo in caso di herpes, viola per l'Hpv, blu per la sifilide e verde per la clamidia. Questo preservativo non è ancora in produzione ma gli studenti che lo hanno ideato non vedono l'ora che progetto diventi realtà.
Nel frattempo l'intuizione relativa al preservativo che cambia colore per la presenza di virus e batteri è valsa loro il premio salute in occasione dell'iniziativa TeenTech dedicata alle idee innovative presentate dagli studenti in Gran Bretagna. I quattro ragazzi che lo hanno ideato frequentano la Isaac Newton Academy di Ilford, nell'Essex.
Con il preservativo intelligente si possono individuare ceppi di batteri differenti. Una volta al corrente del problema, la coppia potrà rivolgersi ad un medico per comprendere come intervenire, per ricevere una diagnosi e una cura.
Se le aziende troveranno interessante la novità, presto o tardi potremo trovare in vendita questo preservativo intelligente pensato anche per contribuire a diffondere una maggiore consapevolezza sulle malattie a trasmissione sessuale. '''
},


{
    "_id" : "http://ilmanifesto.info/olio-di-palma-e-arrivata-lora-della-verita/", 
    "title" : "Olio di palma, è arrivata l’ora della verità", 
    "subTitle" : "Palma leaks. Il recente dossier dell'Agenzia europea Esfa conferma la pericolosità del grasso tropicale: cancerogeno e genotossico. E Big Food sapeva" ,
    "data" : "15-06-2016",
    "evaluation": 2  ,
    "body" : '''
Big Food sapeva da 12 anni che l’olio di palma usato negli alimenti porta con sé contaminanti tossici e cancerogeni. Dai documenti delle autorità e delle multinazionali del cibo emerge chiaramente che i rischi correlati al consumo dello scadente grasso vegetale erano noti. Che se ne è parlato a lungo in quegli ambienti, ma senza porre rimedio.
A diffondere le slide del Palma-Leaks è GIFT Great Italian Food Trade, un portale web in 8 lingue, che promuove nel mondo il cibo made in Italy di qualità, sostenibile e accessibile. “Non serve scomodare Julian Assange, basta una breve ricerca sul web per scoprire che Big Food sapeva dei rischi correlati al consumo di olio di palma. Tuttavia, all’insegna del maggior profitto, ne ha incrementato l’utilizzo, raddoppiandolo in pochi anni”, dice il fondatore del portale, Dario Dongo, avvocato esperto di sicurezza alimentare, autore della petizione per bandire ol’olio di palma dagli alimenti, lanciata assieme al Fatto alimentare e sostenuta da 176mila firme.
Le prove raccolte da GIFT parlano chiaro. La prima traccia sulla tossicità del palma risale al 2004, quando l’Università di Praga descrive la presenza di contaminanti tossici (3-mpcd) negli alimenti trasformati. Dopo tre anni il Centro per la sicurezza alimentare di Stoccarda (CVUA), analizza 400 alimenti e scopre livelli significativi di contaminanti tossici nei prodotti contenenti olio di palma (prodotti per l’infanzia, cracker e barrette). Lo stesso anno, l’Autorità tedesca per la sicurezza alimentare evidenzia la necessità di ridurre le sostanze cancerogenere negli alimenti per la prima infanzia.
A livello industriale, l’eco si manifesta 7 anni fa. Nel 2009, in due occasioni, i rappresentanti di Nestlé espongono ai convegni di categoria i rischi correlati all’olio tropicale. Gli atti di un convegno organizzato da Ilsi, un centro di ricerca di Bruxelles finanziato dalle multinazionali dell’alimentare, documentano che la presenza di contaminanti tossici è particolarmente elevata nell’olio di palma raffinato. Tutti sapevano. Ma hanno continuato a impiegare dosi sempre maggiori del pericoloso grasso tropicale, senza risolverne le criticità.
A farle emergere è stata l’Agenzia europea per la sicurezza alimentare (Efsa), che meno di due settimane fa, il 3 maggio 2016, ha pubblicato un corposo dossier in cui avverte che i contaminanti presenti nell’olio di palma sono cancerogeni e genotossici (i danni al Dna si trasmettono alla eventuale prole).
Per uno di questi (3-mcpd) l’Efsa ha fissato una soglia di tollerabilità di 0,38 microgrammi per chilo di peso corporeo: una dose oggi ampiamente superata dalla popolazione, in particolare da bambini, adolescenti e persino lattanti.
Dopo gli sforzi e gli investimenti pubblicitari spesi per riabilitare l’olio di palma, l’industria è al capolinea.
La combriccola dei produttori asiatici della materia prima e delle multinazionali alimentari che lo impiegano ha pure creato un marchio per l’olio di palma “sostenibile”, di dubbio significato, secondo la ong Supply-Change. Poi ha contrastato l’allarme contro il palma concausa di obesità, sostenendo che la dose fa il veleno, basta mangiare con equilibrio.
Ora ha finito gli argomenti.
Dopo il parere dell’Efsa ha giocato un ultimo tentativo di disinformazione: i contaminanti di processo sono anche in altri oli vegetali raffinati, ha provato a sostenere. Ma il dossier parla chiaro: nel palma ve ne sono da 6 a 10 volte di più.
Per lo scadente grasso tropicale è arrivato il momento della verità. Il primo giro è tutto uno scaricabarile. L’industria si rivolge al ministro della Salute. Il ministro Lorenzin passa la palla al commissario europeo. A Strasburgo i parlamentari cominciano a destarsi.
Le critiche al massiccio impiego di olio di palma negli alimenti industriali, del resto, non sono nuove. Nell’opinione pubblica hanno registrato un coinvolgimento crescente, via via che i danni correlati hanno stretto il cerchio e si sono fatti più vicini.
All’inizio, circa 6 anni fa, si lottava contro le coltivazioni delle palme da olio nei paesi tropicali per il loro impatto distruttivo sull’ambiente e sulle comunità locali, che pagano per primi il prezzo del lucroso business, subendo soprusi ben documentati dalle ong contro il land grabbing (la rapina delle terre), soprattutto in Africa e Sud-est asiatico. Le dimensioni del fenomeno sembrano incontenibili: tra il 2008 e il 2014, nei paesi in via di sviluppo gli investitori stranieri hanno conquistato 56 milioni di ettari di terre, un’estensione pari alla Francia.
Alla fine del 2014, con l’entrata in vigore del regolamento europeo 1169/11 che impone trasparenza sulle etichette alimentari si è scoperto che l’olio di palma (fino ad allora nascosto in etichetta sotto l’enigmatica dicitura “grassi o oli vegetali”) è dappertutto. È aggiunto nei biscotti, nei cracker, nelle merendine. È usato in ristoranti, pizzerie, friggitorie e fast-food, pasticcerie. Sta nei dadi, nei piatti pronti, persino nelle bevande servite al bar, come il “caffè al ginseng”. Anche in molti prodotti biologici.
Una pervasività che ha acceso l’attenzione sul consumo eccessivo e sui rischi correlati (obesità, diabete e malattie cardiovascolari). L’olio di palma contiene infatti almeno il 50% di grassi saturi, la stessa quantità del burro, ma a differenza di quest’ultimo è insapore ed economico, quindi pressoché onnipresente. Secondo il parere espresso due mesi fa dall’Istituto superiore di sanità, in Italia ne assumiamo ben 12 grammi al giorno. E i bambini superano del 49% la dose giornaliera di grassi saturi, proprio a causa del consumo elevato di prodotti contenenti olio di palma.Ma le foreste pluviali sono lontane e i grassi saturi fanno poca paura. Sapere che un alimento tanto mangiato dai bambini contiene sostanze che provocano il cancro rimette ora al centro la battaglia contro il palma.
Una versione ridotta di questo articolo è uscita sul manifesto in edicola il 15 maggio 2016.    '''
},

{
    "_id" : "https://www.efsa.europa.eu/it/press/news/160503a", 
    "title" : "Contaminanti da processo in oli vegetali e alimenti", 
    "subTitle" : "I contaminanti da processo a base di glicerolo presenti nell’olio di palma, ma anche in altri oli vegetali, nelle margarine e in alcuni prodotti alimentari trasformati, suscitano potenziali problemi di salute per il consumatore medio di tali alimenti di tutte le fasce d’età giovane e per i forti consumatori di tutte le fasce d’età." ,
    "data" : "03-04-2016",
    "evaluation":  1 ,
    "body" : '''L'EFSA ha valutato i rischi per la salute pubblica derivanti dalle sostanze: glicidil esteri degli acidi grassi (GE), 3-monocloropropandiolo (3-MCPD), e 2-monocloropropandiolo (2-MCPD) e relativi esteri degli acidi grassi. Tali sostanze si formano durante le lavorazioni alimentari, in particolare quando gli oli vegetali vengono raffinati ad alte temperature (circa 200° C).
I più elevati livelli di GE, come pure di 3-MCPD e 2-MCPD (compresi gli esteri) sono stati riscontrati in oli di palma e grassi di palma, seguiti da altri oli e grassi. Per i consumatori a partire dai tre anni di età, margarine e 'dolci e torte' sono risultati essere le principali fonti di esposizione a tutte le sostanze.
Glicidil esteri degli acidi grassi: genotossici e cancerogeni
Il gruppo di esperti scientifici dell’EFSA sui contaminanti nella catena alimentare (CONTAM) ha esaminato le informazioni sulla tossicità del glicidolo (composto precursore dei GE) per valutare il rischio dai GE, ipotizzando una conversione completa degli esteri in glicidolo dopo l'ingestione.
La dott.ssa Helle Knutsen, presidente del gruppo CONTAM, ha detto: "Ci sono evidenze sufficienti che il glicidolo sia genotossico e cancerogeno, pertanto il gruppo CONTAM non ha stabilito un livello di sicurezza per i GE".
Nel valutare le sostanze genotossiche e cancerogene che sono presenti accidentalmente nella catena alimentare, l'EFSA calcola un cosiddetto 'margine di esposizione' per i consumatori. In generale, più tale margine è elevato, più si riduce il livello di preoccupazione per i consumatori.
Il gruppo ha concluso che i GE costituiscono un potenziale problema di salute per tutte le fasce d’età più giovani e mediamente esposte, nonché per i consumatori di tutte le età con esposizione elevata.
 "L'esposizione ai GE dei neonati che consumino esclusivamente alimenti per lattanti costituisce motivo di particolare preoccupazione, in quanto è fino a dieci volte il livello considerato a basso rischio per la salute pubblica", ha detto la dott.ssa Knutsen.
La disamina del gruppo ha messo in luce che i livelli di GE negli oli e grassi di palma si sono dimezzati tra il 2010 e il 2015, grazie a misure adottate volontariamente dai produttori. Ciò ha determinato una diminuzione importante dell’esposizione dei consumatori a dette sostanze.
Esposizione a 3-MCPD superiore al livello di sicurezza; dati insufficienti su 2-MCPD
"Abbiamo stabilito una dose giornaliera tollerabile (DGT) di 0,8 microgrammi per chilogrammo di peso corporeo al giorno (µg/kg di peso corporeo/giorno) per 3-MCPD e i relativi esteri degli acidi grassi sulla base delle prove che collegano questa sostanza a un danno d'organo nei test sugli animali, "ha spiegato la dott.ssa Knutsen, che ha poi aggiunto che "le informazioni tossicologiche sono tuttavia troppo limitate per stabilire un livello sicuro per 2-MCPD".
La stima delle esposizioni medie ed elevate al 3-MCPD di entrambe le forme per le fasce di età più giovani, adolescenti compresi (fino ai 18 anni di età), supera la DGT e costituisce un potenziale rischio per la salute.
L'olio di palma contribuisce in maniera rilevante all’esposizione a 3-MCPD e 2-MCPD nella maggior parte dei soggetti. I livelli di 3-MCPD e dei suoi esteri degli acidi grassi negli oli vegetali sono rimasti sostanzialmente invariati negli ultimi cinque anni.
Quali sono i prossimi passi?
Questa valutazione del rischio fornirà informazioni utili ai gestori del rischio della Commissione europea e degli Stati membri, i quali regolamentano la sicurezza alimentare nell'UE. Essi utilizzeranno la consulenza scientifica dell'EFSA per riflettere su come gestire i potenziali rischi per i consumatori legati all'esposizione a tali sostanze negli alimenti. Il gruppo scientifico ha inoltre espresso una serie di raccomandazioni affinché si conducano ulteriori ricerche per colmare le lacune nei dati e migliorare le conoscenze sulla tossicità di queste sostanze, in particolare di 2-MCPD, e sull’esposizione dei consumatori ad essi tramite l’alimentazione. '''
},

{
    "_id" : "http://www.tgcom24.mediaset.it/mondo/india-80-suicidi-nei-primi-tre-mesi-del-2016-un-villaggio-accusa-il-demonio-_3007414-201602a.shtml", 
    "title" : "India, 80 suicidi nei primi tre mesi del 2016: un villaggio accusa il demonio", 
    "subTitle" : "Scettiche le autorità mediche: uno psichiatra ha infatti puntato il dito contro lʼalto tasso di pesticidi usati nei campi e contro lo stress finanziario" ,
    "data" : "08-05-2016",
    "evaluation":  1 ,
    "body" : '''
A Badi sono certi: c’è una “presenza demoniaca” che spinge le persone a togliersi la vita. Questo villaggio di 2.500 abitanti del Madhya Pradesh, in India, è balzato all’onore delle cronache per l’insolita quantità di suicidi: solo nei primi tre mesi del 2016 si sono uccise 80 persone. A riportarlo è il Times of India. “Ci sono 320 famiglie nel nostro villaggio e almeno una persona da ognuna di queste si è uccisa”, ha spiegato un funzionario locale.
Rajendra Sisodiya, il “sarpanch” (“capovillaggio”), si dice certo che sia una “presenza demoniaca” a produrre questi suicidi. Lui stesso è dovuto subentrare nell’incarico al cugino, Jeevan, che si impiccato a un albero di fronte alla casa. Anche la madre e il fratello del “sarpanch” si sono tolti la vita.
Psichiatra: “Colpa dei pesticidi” – Alla pista demoniaca non credono tuttavia le autorità mediche. Srikanth Reddy, uno psichiatra, spiega che questi suicidi potrebbero essere dovuti a episodi depressivi e schizofrenici legati, probabilmente, all’alto tasso di pesticidi usati nei campi e a stress finanziario. In particolare, il medico ha segnalato uno studio in Cina, che aveva individuato la causa della depressione in diversi contadini suicidi in alcuni insetticidi contenenti organofosfati, che sono anche alla base di alcuni gas nervini.    '''
},

{
    "_id" : "http://www.lastampa.it/2013/04/22/scienza/benessere/gravidanza-parto-pediatria/la-bambina-miracolo-guarisce-improvvisamente-da-leucemia-e-sindrome-di-down-ReG4aQDYB66GgN8EDGHEQI/pagina.html", 
    "title" : "La bambina miracolo: guarisce improvvisamente da leucemia e sindrome di Down", 
    "subTitle" : "Nata prematura, a 28 settimane, con diversi problemi a cuore e occhi, la piccola Clara era anche affetta da leucemia e sindrome di Down. Dopo una breve cura, leucemia e trisomia 21 sono misteriosamente sparite" ,
    "data" : "22-04-2013",
    "evaluation":  2 ,
    "body" : ''' 
    I medici sono rimasti allibiti. I genitori sono al settimo cielo.
E’ la storia della “bambina miracolo”: Clara McLoughlin, nata a Dublino (Irlanda) il 7 agosto 2012 a ventotto settimane di gravidanza. Un parto prematuro che vedeva coinvolto anche il fratellino gemello che, tuttavia, non è sopravvissuto.
Quando è nata pesava soltanto 1,14 chili, presentava diversi problemi agli occhi e perfino un buco nel cuore – che dovevano essere curati. Ma questo, rispetto a quanto scoperto poco dopo, poteva essere il meno: la neonata, infatti, era anche affetta da leucemia e risultata positiva alla trisomia 21 (la sindrome di Down). A motivo di ciò, riporta l’Indipendent, la bambina è stata trasferita all’Unità Intensiva Neonatale del Rotunda hospital.
«I medici – dichiara la mamma Helen Kavanagh – non ci avevano dato molte speranze e non si aspettavano che sopravvivesse, e valutavano la situazione ora per ora. Hanno consultato dei consulenti per osservare il suo caso, perché era così unico».
Per poter offrire una qualche cura, i medici hanno somministrato una dose di farmaco dopo soli due giorni dalla nascita, al fine di attaccare le cellule cancerogene della leucemia. Dopo questo primo intervento, la bambina ha reagito e iniziato a mettere su peso.
Nel frattempo, la piccola Clara era anche stata sottoposta a un intervento laser per curare gli occhi e un’operazione per riparare un buco nel cuore – una situazione globale apparentemente disperata.
Nel mese di novembre 2012 e in seguito nel febbraio 2013, i medici hanno eseguito dei test genetici al fine di monitorare lo stato della leucemia e la trisomia 21.
A questo punto sono iniziate le sorprese: la leucemia era sparita, rivelandosi una condizione transitoria – anche se non spiegabile. Se la leucemia era “misteriosamente” svanita, di certo sarebbe rimasta la sindrome di Down, poiché un fattore cromosomico non può in teoria comportarsi allo stesso modo. 
Doppia sorpresa: anche la trisomia 21 non c’era più. Era come «se il gene avesse lasciato il suo corpo», racconta la mamma di Clara.
Ora, gli scienziati stanno tentando ci capire come la bambina abbia sviluppato sia la leucemia che la sindrome di Down – ma soprattutto come sia possibile che tutte e due queste condizioni siano sparite senza lasciare apparentemente traccia. La bambina, infatti, dopo 91 giorni di ospedale è tornata a casa e, come dichiarato dai coniugi McLoughlin, sta benissimo, pesa 20 chili, cresce ed è vivace.
«E’ molto difficile per le persone che non l’hanno vista, credere a tutto ciò – aggiunge la mamma – ma lei è perfetta e felice».
    '''
}]


from time import time
class Counter:
    def __init__(self , tick= 50):
        self.i = 0
        self.tick = tick
        self.t = time()
        print "started"
        
    def update(self):
        self.i+=1
        if(self.i% self.tick == 0):
            print self.i , " ----> " , time() - self.t
            self.t = time()
            
for x in data:
    x["evaluation"] = 1
    
from pymongo import MongoClient as MC
client = MC()
db = client.rumors2
c = Counter()
for x in data:
    c.update()
    try:
        db.raw.insert_one(x)
    except Exception:
        print "can not insert"