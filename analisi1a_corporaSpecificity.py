from pymongo import MongoClient as MC
from gensim import corpora
from util import Counter
import re

def funct(s):
    return (s[1] - s[0])/ (s[0]**.5)
    
if __name__ == "__main__":
    client = MC()
    db = client.rumors2
    
    
    db.specifCorpora.drop()
    regx = re.compile("/.*cicap.*/")
    agg1 = [
    {"$match":{"_id": {"$not": regx}}},
    {"$unwind" : "$words"},
    {"$group" : {
                "_id": "$evaluation" , 
                "tokens" : {"$push":"$words"},
                "tokenset" : {"$addToSet":"$words"},
                "ntokens"  : {"$sum" : 1} 
                }
    },
    { "$out" : "specifCorpora"}
    ]
    db.wordsXdocs.aggregate(agg1)
    
    
    db.specifCorporaTokenN.drop()
    agg2 = [
    {"$project":{"_id":1 , "tokens" : 1 , "ntokens"  :1 }},
    {"$unwind" : "$tokens"},
    {"$group" : {
                "_id": {"token" : "$tokens" , "eval" : "$_id"},
                "num" : {"$sum" : 1} 
                
                }
    },
    { "$out" : "specifCorporaTokenN"}
    ]
    db.specifCorpora.aggregate(agg2)

    
    totals = { x["_id"]:x["ntokens"] for x in db.specifCorpora.aggregate([{"$project":{"ntokens":1}}])}
    specs = {}
    c = Counter()
    print "until",db.specifCorporaTokenN.count()
    for w in db.specifCorporaTokenN.find():
        c.update()
        if(w["_id"]["token"] not in specs.keys()):
            specs[w["_id"]["token"]] = {0:1.0/(totals[0]+1),1:1.0/(totals[1]+1)}
        specs[w["_id"]["token"]][w["_id"]["eval"]] =  float(w["num"] +1.0 )/ (totals[w["_id"]["eval"]]+1.0)
    
    
    print "evaluating dataset"
    dataset = [ (s , funct(v)) for s,v in specs.items()]
    print "writing dataset"
    f = open("output/specificity_terms.csv","w")
    f.write("keywords,spec\n")
    for k,s in  dataset:
        f.write('"{}",{}\n'.format(k,s))
    f.close()
    print "writing in database"
    db.feature1Specificity.drop()
    todump = [ {"token":k , "spec":v} for k,v in dataset ]
    db.feature1Specificity.insert(todump)