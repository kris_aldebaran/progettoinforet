from pymongo import MongoClient as MC
from gensim import corpora , models, matutils
from util import Counter
''' 
'''

if __name__ == "__main__":
    client = MC()
    db = client.rumors2
    print "creating dictionary"
    texts = [ (x["_id"] , x["words"] , x["evaluation"]) for x in db.wordsXdocs.find()]
    words = [x[1] for x in texts]
    dct = corpora.Dictionary(words)
    print "saving dictionary"
    dct.save('./tmp/dictonary.dict')
    n = len(dct.token2id.keys())
    print "creating corpus"
    corpus = [dct.doc2bow(text) for text in words]
    print "saving corpus"
    corpora.MmCorpus.serialize('./tmp/corpus.mm', corpus)  # store to disk, for later use
    tfidf = models.TfidfModel(corpus)
    data =  [tfidf[x] for x in corpus]
    out  = matutils.corpus2dense(data,n ).T 
    f = open("dataset/dataset_tfidf.csv","w")
    f.write("id,")
    for i in range(n):
        f.write(str(i)+",")
    f.write("label\n")
    c = Counter(tick = 50)
    for id,tf,label in zip([x[0] for x in texts] , out  ,[x[2] for x in texts] ):
        c.update()
        f.write('"{}",'.format(id))
        for x in tf:
            f.write(str(x)+",")
        f.write("{}\n".format(label))