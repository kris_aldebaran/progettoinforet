from sklearn.naive_bayes import GaussianNB
from random import shuffle
from util import Counter , Kappa
from pymongo import MongoClient as MC
from math import e
client = MC()
db = client.rumors2
alpha = 0.3
def spec(l):
    # print l
    rs = db.wordsXdocs.find({"_id":l[1:-1]})
    # print rs
    if(rs.count()<=0):
        return 0.5
    tokens = rs[0]["words"]
    feat1 = db.feature1Specificity.find({"token":{"$in":tokens}})
    val = sum([ x["spec"] for x in feat1 ])
    return (1.0)/(1.0+ (e**(-val)))

print "data reading and creation"
f = open("dataset/dataset_tfidf.csv")
data = [line.split(",") for line in list(f)[1:] if(line != "")]
shuffle(data)
Y = [0 if float(x[-1]) <= 0 else 1 for x in data]
# X = [[float(v) for v in x[:-1]] for x in data]
X = []
c = Counter(tick = 200)
for x in data:
    c.update()
    temp = [float(v) for v in x[1:-1]]
    X.append(temp)
links = [x[0] for x in data]

vp = 0
fp = 0
vn = 0
fn = 0

n = len(data)     
train_A , Y_A = X[:n/2] , Y[:n/2] 
train_B , Y_B = X[n/2:] , Y[n/2:] 

print "step 1"
nb = GaussianNB()
nb.fit(train_A,Y_A)
c = Counter(tick = 50)
for link , pred, y in zip(links, nb.predict_proba(train_B) , Y_B):
    
    c.update()
    val = (pred[1] + spec(link))/2

    if(val>0.5 and y>0):
        vp += 1
    elif (val>0.5 and y<=0):
        fp += 1
    elif (val<=0.5 and y<=0):
        vn += 1
    elif (val<=0.5 and y>0):
        fn += 1
        
print "step 2"       
nb = GaussianNB()
nb.fit(train_B,Y_B)
c = Counter(tick = 50)
for link , pred, y in zip(links,nb.predict_proba(train_A) , Y_A):
    
    c.update()
    val = (alpha*pred[1] + (1-alpha)*spec(link))/(alpha+(1-alpha))
    if(val>0.5 and y>0):
        vp += 1
    elif (val>0.5 and y<=0):
        fp += 1
    elif (val<=0.5 and y<=0):
        vn += 1
    elif (val<=0.5 and y>0):
        fn += 1


print "veri positivi: ", vp
print "falsi positivi: ", fp
print "veri negativi: ", vn
print "false negativi: ", fn
print "errore: " , 100 - 100*(vp+vn+0.0)/(vp+vn+0.0+fn+fp) , " %"
print "precision" , 100*(vp+0.0)/(vp+fp)
print "recall" , 100*(vp+0.0)/(vp+fn)
print "kappa" , Kappa(vp,fn,fp,vn)        