from pymongo import MongoClient as MC
from gensim import corpora , models, matutils
from util import Counter
from collections import defaultdict

def is_rumor(eval,id):
    return eval == 1
def is_not_rumor(eval,id):
    return eval != 1
def is_ansa(eval,id):
    return "ansa" in id
def is_cicap(eval,id):
    return "cicap" in id
def writeCSV(f,texts,dct,model,test=is_rumor , pos = 0):
    out = defaultdict(int)
    c = Counter(tick = 50)
    for text in texts:
        c.update()
        if(test(text[2],text[0])):
            data = model[dct.doc2bow(text[1])]
            for k , v in data:
                out[dct[k]] += v
    finals = out.items()
    finals.sort(key=lambda x: -x[1])
    for k,v in finals:
        f.write('"{}",{},{},{},{},{}\n'.format(k,
        1 if pos == 0 else 0 ,
        1 if pos == 1 else 0 ,
        1 if pos == 2 else 0 ,
        1 if pos == 3 else 0 ,
        v))

if __name__ == "__main__":
    client = MC()
    db = client.rumors2
    print "creating dictionary"
    texts = [ (x["_id"] , x["words"] , x["evaluation"]) for x in db.wordsXdocs.find()]
    words = [x[1] for x in texts]
    dct = corpora.Dictionary(words)
    print "saving dictionary"
    dct.save('./tmp/dictonary.dict')
    n = len(dct.token2id.keys())
    print "creating corpus"
    corpus = [dct.doc2bow(text) for text in words]
    print "saving corpus"
    corpora.MmCorpus.serialize('./tmp/corpus.mm', corpus)  # store to disk, for later use
    tfidf = models.TfidfModel(corpus)
    
    print "writing data"
    f = open("output/analisi_tfidf.csv","w")
    f.write("Termine,rumors,no_rumors,ansa,cicap,TF_IDF\n")
    
    
    writeCSV(f,texts,dct,tfidf,test=is_rumor , pos = 0)
    writeCSV(f,texts,dct,tfidf,test=is_not_rumor , pos =1)
    writeCSV(f,texts,dct,tfidf,test=is_ansa , pos =2)
    writeCSV(f,texts,dct,tfidf,test=is_cicap , pos =3)
    
   
    
    
    
    f.close()
            
         
    
    