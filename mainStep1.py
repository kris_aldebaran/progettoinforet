from time import time
from pymongo import MongoClient as MC
from copy import copy
import string
import nltk
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from util import Counter
'''
Questo codice prende i dati in rumors.raw e esegue 
    - ripulitura unicode
    - tokenizzazione
    - rimozione stop word
    - stemming
'''
LENCACHE = 100

maps = (
    ( u"\u0300" , " "  ),
    ( u"\u2019" , " "  ),
    ( u" \xe8 " , " essere "),
    ( u"\xe9"   , "e"  ),
    ( u"\xe8"   , "e"  ),
    ( u" \xc8 " , " Essere "),
    ( u"\xc9"   , "E"  ),
    ( u"\xc8"   , "E"  ),
    ( u"\xe0"   , "a"  ),
    ( u"\xe1"   , "a"  ),
    ( u"\u0103"   , "a"  ),
    ( u"\u0163"   , "t"  ),
    ( u"\xae"   , "'"  ),
    ( u"\xc0"   , "A"  ),
    ( u"\xc1"   , "A"  ),
    ( u"\xf2"   , "o"  ),
    ( u"\xf3"   , "o"  ),
    ( u"\xf9"   , "u"  ),
    ( u"\xfa"   , "u"  ),
    ( u"\xec"   , "i"  ),
    ( u"\xed"   , "i"  ),
    ( u"\xCc"   , "I"  ),
    ( u"\xCd"   , "I"  ),
    ( u"\xb2"   , " 2 "  ),
    ( u"\u201c"   , " "  ),
    ( u"\u201d"   , " "  ),
    ( u"\u2013"   , "-"  ),
    ( u"\xab"   , " "  ),
    ( u"\xbb"   , " "  ),
    ( u"\u2009"   , " "  ),
    ( u"\xb0"   , " "  ),
    ( u"\u2026"   , "..."  ),
    ( u"\u02bc"   , "'"  ),
    ( u"\xb5"   , " "  ),
    ( u"\xa0"   , " "  ),
    ( u"\u2014"   , "_"  ),
    ( u"\u2018"   , "'"  ),
    ( u"\xae"   , "'"  ),
    
)

def remove_unicode(s):
    x = copy(s)
    for a,b in maps:
        while a in x:
            x = x.replace(a,b)
    return str(''.join([i for i in x if ord(i)<128]))
    
def tokenize(text):
    tokens = tokens = nltk.word_tokenize(text)
    toret = []
    i = 0
    while(i< len(tokens)):
        actual = tokens[i]
        if(actual!= "" and actual not in [t for t in string.punctuation]+["''"] + ["``"]):
            if(i+1<len(tokens) and tokens[i+1] == "%"):
                toret.append(actual+tokens[i+1])
            elif(actual[0] == actual[0].upper()):
                toret.append(actual[0] +actual[1:].lower())
            else:
                toret.append(actual)
        i+=1        
    return toret
def process(data):
    toret ={}
    s = remove_unicode(data["body"])
    if("title" in data.keys()):
        s+=" " + remove_unicode(data["title"])
    if("subTitle" in data.keys()):
        s+=" " + remove_unicode(data["subTitle"])
    if("data" in data.keys()):
        toret["data"] = data["data"]
    if("link" in data.keys()):
        toret["link"] = data["link"]
    toret["_id"] = data["_id"]
    toret["evaluation"] = 1 if int(data["evaluation"]) > 0 else 0
    tokens = tokenize(s)
    it_stopwords = [x for x in stopwords.words('italian')]
    tokens = [x for x in tokens if not x.lower() in it_stopwords]
    stemmer = SnowballStemmer('italian')
    tokens =  [stemmer.stem(x) for x in tokens]
    toret["words"] = tokens
    return toret

if __name__ == "__main__":
    client = MC()
    db = client.rumors2
    # drop table
    db.wordsXdocs.drop()
    cache =[]
    c = Counter(tick = 20)
    for data in db.raw.find():
        c.update()
        cache.append(process(data))
        if(len(cache) > LENCACHE ):
            print "writing on db"
            db.wordsXdocs.insert(cache)
            cache = []
    if(len(cache) > 0 ):
        print "writing on db"
        db.wordsXdocs.insert(cache)