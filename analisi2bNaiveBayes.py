from sklearn.naive_bayes import GaussianNB
from random import shuffle
from util import Counter , Kappa
print "data reading and creation"
f = open("dataset/dataset_tfidf.csv")
data = [line.split(",")[1:] for line in list(f)[1:] if(line != "")]
shuffle(data)
Y = [0 if float(x[-1]) <= 0 else 1 for x in data]
# X = [[float(v) for v in x[:-1]] for x in data]
X = []
c = Counter(tick = 200)
for x in data:
    c.update()
    temp = [float(v) for v in x[:-1]]
    X.append(temp)

vp = 0
fp = 0
vn = 0
fn = 0

n = len(data)     
train_A , Y_A = X[:n/2] , Y[:n/2] 
train_B , Y_B = X[n/2:] , Y[n/2:] 

print "step 1"
nb = GaussianNB()
nb.fit(train_A,Y_A)
c = Counter(tick = 50)
for val, y in zip(nb.predict(train_B) , Y_B):
    c.update()
    # val = nb.predict(x)
    if(val>0 and y>0):
        vp += 1
    elif (val>0 and y<=0):
        fp += 1
    elif (val<=0 and y<=0):
        vn += 1
    elif (val<=0 and y>0):
        fn += 1
        
print "step 2"       
nb = GaussianNB()
nb.fit(train_B,Y_B)
c = Counter(tick = 50)
for val, y in zip(nb.predict(train_A) , Y_A):
    c.update()
    # val = nb.predict(x)
    if(val>0 and y>0):
        vp += 1
    elif (val>0 and y<=0):
        fp += 1
    elif (val<=0 and y<=0):
        vn += 1
    elif (val<=0 and y>0):
        fn += 1


print "veri positivi: ", vp
print "falsi positivi: ", fp
print "veri negativi: ", vn
print "false negativi: ", fn
print "errore: " , 100 - 100*(vp+vn+0.0)/(vp+vn+0.0+fn+fp) , " %"
print "precision" , 100*(vp+0.0)/(vp+fp)
print "recall" , 100*(vp+0.0)/(vp+fn)
print "kappa" , Kappa(vp,fn,fp,vn)        